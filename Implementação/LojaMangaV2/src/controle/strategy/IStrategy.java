package controle.strategy;

import modelo.dominio.EntidadeDominio;

public interface IStrategy {

	public String processar(EntidadeDominio entidade);
}
