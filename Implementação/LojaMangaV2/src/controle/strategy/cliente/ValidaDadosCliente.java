
package controle.strategy.cliente;


import java.text.ParseException;
import java.util.Date;

import controle.strategy.IStrategy;
import modelo.dominio.Cliente;
import modelo.dominio.EntidadeDominio;


public class ValidaDadosCliente implements IStrategy{

    @Override
    public String processar(EntidadeDominio entidade) {
        Cliente cliente = (Cliente) entidade;
        StringBuilder msg = new StringBuilder();

        if(cliente.getDataNascimento() == null) {
            msg.append("Data invalida.\n");
        }

        if(cliente.getNomeCompleto().trim().equals("") || cliente.getNomeCompleto()== null){
            msg.append("O Nome precisa ser preenchido.\n");
        }

        if(cliente.getEmail().trim().equals("") || cliente.getEmail() == null){
            msg.append("O E-mail precisa ser preenchido.\n");
        }

        if(cliente.getCpf().trim().equals("") || cliente.getCpf() == null){
            msg.append("O CPF precisa ser preenchido.\n");
        }   
        
        return msg.toString();
    }
    
}
