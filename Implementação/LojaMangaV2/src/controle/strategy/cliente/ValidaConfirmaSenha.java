
package controle.strategy.cliente;

import controle.strategy.IStrategy;
import modelo.dominio.Cliente;
import modelo.dominio.EntidadeDominio;

public class ValidaConfirmaSenha implements IStrategy {

    @Override
    public String processar(EntidadeDominio entidade) {
        Cliente cliente = (Cliente)entidade;
        if(!(cliente.getSenha().getSenha().equals(cliente.getSenha().getConfirmaSenha()))) {
             return "As duas senhas devem coincidir";
        }
        return null;
    }
    
}
