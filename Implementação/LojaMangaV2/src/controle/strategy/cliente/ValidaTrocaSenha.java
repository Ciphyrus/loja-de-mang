
package controle.strategy.cliente;

import controle.strategy.IStrategy;
import modelo.dao.ClienteDAO;
import modelo.dominio.Cliente;
import modelo.dominio.EntidadeDominio;
import modelo.dominio.Senha;


public class ValidaTrocaSenha implements IStrategy {

    @Override
    public String processar(EntidadeDominio entidade) {
        Senha senha = (Senha) entidade;
        Cliente clienteID = new Cliente();
        clienteID.setId(senha.getId());
        clienteID.setNomeCompleto("");
        if (!senha.getSenhaTemp().equals("") || senha.getSenhaTemp() != null) {
            Cliente senhaAntiga;
            
            // Consulta se a senha antiga e igual a senha nova
            ClienteDAO dao = new ClienteDAO();
            senhaAntiga = (Cliente) dao.consultar(clienteID).getEntidades().get(0);
            if (!senha.getSenhaTemp().equals(senhaAntiga.getSenha().getSenha())) {
                return "A senha atual esta incorreta.";
            }
            
            // Verifica se a senha e a Confirmacao da Senha sao iguais
            if (!(senha.getSenha().equals(senha.getConfirmaSenha()))) {
                return "As duas senhas devem coincidir";
            }
            return null;
        } else {
            return "A senha original e obrigatoria";
        }
    }

}
