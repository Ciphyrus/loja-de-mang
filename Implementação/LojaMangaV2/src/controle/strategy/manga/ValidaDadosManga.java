
package controle.strategy.manga;


import java.text.ParseException;
import java.util.Date;

import controle.strategy.IStrategy;
import modelo.dominio.EntidadeDominio;
import modelo.dominio.Manga;

public class ValidaDadosManga implements IStrategy {

    @Override
    public String processar(EntidadeDominio entidade) {
        Manga produto = (Manga) entidade;
        StringBuilder msg = new StringBuilder();

        if (produto.getTitulo().equals("") || produto.getTitulo() == null) {
            msg.append("O nome do produto eh obrigatorio.");
        }

        if (produto.getSinopse().equals("") || produto.getSinopse() == null) {
            msg.append("A descrição do produto eh obrigatorio.");
        }

        if (produto.getPeso() == 0.0) {
            msg.append("O peso do produto eh obrigatorio.");
        }

        if (produto.getPreco() == 0.0) {
            msg.append("O preco do produto eh obrigatorio.");
        }

        /*if (produto.getEndereco_imagem().equals("") || produto.getEndereco_imagem() == null) {
            msg.append("A imagem principal do produto eh obrigatorio.");
        }*/
        
        if (produto.getCategorias().get(0) == null) {
            msg.append("A subcategoria do produto eh obrigatorio.");
        }

        return msg.toString();
    }

}
