package controle.strategy.cartao;

import controle.strategy.IStrategy;
import modelo.dominio.CartaoCredito;
import modelo.dominio.EntidadeDominio;

public class ValidaCartao implements IStrategy{

    @Override
    public String processar(EntidadeDominio entidade) {
        CartaoCredito cartao = (CartaoCredito) entidade;
        StringBuilder msg = new StringBuilder();
        
        if(cartao.getBandeira().trim().equals("")){
            msg.append("A bandeira do cartao eh obrigatorio.");
        }
        
        if(cartao.getCodigoSeguranca().trim().equals("")){
            msg.append("O codigo de seguranca eh obrigatorio.");
        }
        
        if(cartao.getNomeImpresso().trim().equals("")){
            msg.append("O nome impresso no cartao eh obrigatorio.");
        }
        
        if(cartao.getNumeroCartao().trim().equals("")){
            msg.append("O numero do cartao eh obrigatorio.");
        }
        
        if(cartao.getValidade().toString().trim().equals("")){
            msg.append("A validade eh obrigatorio");
        }
        
        return msg.toString();
    }
    
}
