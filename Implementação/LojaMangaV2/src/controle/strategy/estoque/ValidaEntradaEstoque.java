package controle.strategy.estoque;

import controle.strategy.IStrategy;
import modelo.dominio.EntidadeDominio;
import modelo.dominio.Estoque;

public class ValidaEntradaEstoque implements IStrategy {

    @Override
    public String processar(EntidadeDominio entidade) {
        Estoque estoque = (Estoque) entidade;
        StringBuilder msg = new StringBuilder();
        
        if (estoque.getQuantidadeEstoque() < 0) {

            msg.append("A quantidade em estoque nao pode ser negativa.");
        }

        return msg.toString();
    }

}
