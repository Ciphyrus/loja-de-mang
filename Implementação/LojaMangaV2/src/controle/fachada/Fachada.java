package controle.fachada;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import controle.strategy.IStrategy;
import controle.strategy.cartao.ValidaCartao;
import controle.strategy.categoria.ValidaDadosCategoria;
import controle.strategy.cliente.CriptografaSenha;
import controle.strategy.cliente.ValidaCPF;
import controle.strategy.cliente.ValidaConfirmaSenha;
import controle.strategy.cliente.ValidaDadosCliente;
import controle.strategy.cliente.ValidaEmailExistente;
import controle.strategy.cliente.ValidaPadraoSenha;
import controle.strategy.cliente.ValidaSenha;
import controle.strategy.cliente.ValidaTrocaSenha;
import controle.strategy.endereco.ValidaEndereco;
import controle.strategy.manga.ValidaDadosManga;
import controle.strategy.pedido.ValidarDataPedido;
import modelo.dao.CarrinhoDAO;
import modelo.dao.CartaoCreditoDAO;
import modelo.dao.CategoriaDAO;
import modelo.dao.ClienteDAO;
import modelo.dao.EnderecoDAO;
import modelo.dao.IDAO;
import modelo.dao.MangaDAO;
import modelo.dao.PedidoDAO;
import modelo.dominio.Carrinho;
import modelo.dominio.CartaoCredito;
import modelo.dominio.Categoria;
import modelo.dominio.Cliente;
import modelo.dominio.Endereco;
import modelo.dominio.EntidadeDominio;
import modelo.dominio.Manga;
import modelo.dominio.Pedido;
import modelo.dominio.Resultado;
import modelo.dominio.Senha;

public class Fachada implements IFachada {
	
	private Map<String, IDAO> daos;

    // Mapa Macro, com TODAS as regras de negocio
    // Observe: ele eh um mapa, de um mapa. E o mapa "menor" tem a lista de strategys
    private Map<String, Map<String, List<IStrategy>>> regrasNegocio;
    
    private StringBuilder sb = new StringBuilder();
    private Resultado resultado;

    public Fachada() {
        // instanciando mapas de daos e regras de negocio macro
        daos = new HashMap<String, IDAO>();
        // Instanciando o mapa macro;
        regrasNegocio = new HashMap<String, Map<String, List<IStrategy>>>();

        // adicionando todas os daos ao hash de daos
        daos.put(Cliente.class.getName(), new ClienteDAO());
        daos.put(Senha.class.getName(), new ClienteDAO());
        daos.put(Endereco.class.getName(), new EnderecoDAO());
        daos.put(CartaoCredito.class.getName(), new CartaoCreditoDAO());
        
        daos.put(Manga.class.getName(), new MangaDAO());
        daos.put(Categoria.class.getName(), new CategoriaDAO());

        daos.put(Pedido.class.getName(), new PedidoDAO());
        daos.put(Carrinho.class.getName(), new CarrinhoDAO());

       

        // --------------------- Hash Cliente ------------------------------//
        // Criando RNs de cliente
        ValidaDadosCliente validaDadosCliente = new ValidaDadosCliente();
        ValidaSenha vSenha = new ValidaSenha();
        ValidaCPF validaCpf = new ValidaCPF();
        ValidaPadraoSenha vPadraoSenha = new ValidaPadraoSenha();
        CriptografaSenha criptografaSenha = new CriptografaSenha();
        ValidaConfirmaSenha validaConfirmaSenha = new ValidaConfirmaSenha();
        ValidaEmailExistente validaEmailExistente = new ValidaEmailExistente();
        
        // Criando lista de RNs do Cliente Salvar
        List<IStrategy> rnsClienteSalvar = new ArrayList<IStrategy>();
        rnsClienteSalvar.add(validaDadosCliente);
        rnsClienteSalvar.add(validaCpf);
        rnsClienteSalvar.add(vSenha);
        rnsClienteSalvar.add(validaConfirmaSenha);
        rnsClienteSalvar.add(vPadraoSenha);
        rnsClienteSalvar.add(criptografaSenha);
        rnsClienteSalvar.add(validaEmailExistente);
        
        // Criando lista de RNs do Cliente Alterar
        List<IStrategy> rnsClienteAlterar = new ArrayList<IStrategy>();
        rnsClienteAlterar.add(validaDadosCliente);
        rnsClienteAlterar.add(validaCpf);

        // Criando mapa de Operacoes Cliente, RNs da operacao de Cliente.
        Map<String, List<IStrategy>> mapaCliente = new HashMap<String, List<IStrategy>>();

        // Adiconando lista de RNs para cada operacao de cliente
        mapaCliente.put("SALVAR", rnsClienteSalvar);
        mapaCliente.put("ALTERAR", rnsClienteAlterar);

        regrasNegocio.put(Cliente.class.getName(), mapaCliente);

        // ------------------------ Hash Senha -------------------------------//
        // Criando RNs de senha
        List<IStrategy> rnsSenhaAlterar = new ArrayList<IStrategy>();
        ValidaTrocaSenha vTrocaSenha = new ValidaTrocaSenha();
        rnsSenhaAlterar.add(vTrocaSenha);

        // Criando mapa de Operacoes Senha, RNs da operacao de Senha.
        Map<String, List<IStrategy>> mapaSenha = new HashMap<String, List<IStrategy>>();

        // Adiconando lista de RNs para cada operacao de cliente
        mapaSenha.put("ALTERAR", rnsSenhaAlterar);
        regrasNegocio.put(Senha.class.getName(), mapaSenha);

        // --------------------------- Hash Endereco -------------------------//
        // Criando RNs de endereco
        ValidaEndereco vEndereco = new ValidaEndereco();

        // Criando lista de RNs do endereco Salvar
        List<IStrategy> rnsEnderecoSalvar = new ArrayList<IStrategy>();
        rnsEnderecoSalvar.add(vEndereco);

        // Criando lista de RNs do endereco Alterar
        List<IStrategy> rnsEnderecoAlterar = new ArrayList<IStrategy>();
        rnsEnderecoAlterar.add(vEndereco);

        // Criando mapa de Operacoes Endereco, RNs da operacao de Endereco.
        Map<String, List<IStrategy>> mapaEndereco = new HashMap<String, List<IStrategy>>();

        // Adiconando lista de RNs para cada operacao de Endereco
        mapaEndereco.put("SALVAR", rnsEnderecoSalvar);
        mapaEndereco.put("ALTERAR", rnsEnderecoAlterar);

        regrasNegocio.put(Endereco.class.getName(), mapaEndereco);

        // ------------------- Hash Cartao de Credito ------------------------//
        // Criando RNs de cartao
        ValidaCartao vCartao = new ValidaCartao();

        // Criando lista de RNs de cartao Salvar
        List<IStrategy> rnsCartaoCreditoSalvar = new ArrayList<IStrategy>();
        rnsCartaoCreditoSalvar.add(vCartao);

        // Criando lista de RNs de cartao Alterar
        List<IStrategy> rnsCartaoCreditoAlterar = new ArrayList<IStrategy>();
        rnsCartaoCreditoAlterar.add(vCartao);

        // Criando mapa de Operacoes CartaoCredito, RNs da operacao de CartaoCredito.
        Map<String, List<IStrategy>> mapaCartaoCredito = new HashMap<String, List<IStrategy>>();

        // Adiconando lista de RNs para cada operacao de Endereco
        mapaCartaoCredito.put("SALVAR", rnsCartaoCreditoSalvar);
        mapaCartaoCredito.put("ALTERAR", rnsCartaoCreditoAlterar);

        regrasNegocio.put(CartaoCredito.class.getName(), mapaCartaoCredito);
        
        // ------------------- Hash Manga ------------------------//
        ValidaDadosManga validaDadosManga = new ValidaDadosManga();
        
        // Criando lista de RNs do Manga Salvar
        List<IStrategy> rnsMangaSalvar = new ArrayList<IStrategy>();
        rnsMangaSalvar.add(validaDadosManga);
        
        // Criando lista de RNs do Manga Alterar
        List<IStrategy> rnsMangaAlterar = new ArrayList<IStrategy>();
        rnsMangaAlterar.add(validaDadosManga);

        // Criando mapa de Operacoes Manga, RNs da operacao de Manga.
        Map<String, List<IStrategy>> mapaManga = new HashMap<String, List<IStrategy>>();

        // Adiconando lista de RNs para cada operacao de manga
        mapaManga.put("SALVAR", rnsMangaSalvar);
        mapaManga.put("ALTERAR", rnsMangaAlterar);
        regrasNegocio.put(Manga.class.getName(), mapaManga);

        // ------------------------ Hash Categoria -------------------------------//
        ValidaDadosCategoria validaDadosCategoria = new ValidaDadosCategoria();
        
        // Criando RNs de categoria Salvar
        List<IStrategy> rnsCategoriaSalvar = new ArrayList<IStrategy>();
        rnsCategoriaSalvar.add(validaDadosCategoria);
        
        // Criando RNs de categoria Alterar
        List<IStrategy> rnsCategoriaAlterar = new ArrayList<IStrategy>();
        rnsCategoriaAlterar.add(validaDadosCategoria);

        // Criando mapa de Operacoes Categoria, RNs da operacao de Categoria.
        Map<String, List<IStrategy>> mapaCategoria = new HashMap<String, List<IStrategy>>();

        // Adiconando lista de RNs para cada operacao de categoria
        mapaCategoria.put("SALVAR", rnsCategoriaSalvar);
        mapaCategoria.put("ALTERAR", rnsCategoriaAlterar);
        regrasNegocio.put(Categoria.class.getName(), mapaCategoria);
        
        // Criando RN para Carrinho Salvar
        List<IStrategy> rnsCarrinhoSalvar = new ArrayList<IStrategy>();
        
        // Criando um Mapa para RN de carrinho
        Map<String, List<IStrategy>> mapaCarrinho = new HashMap<String, List<IStrategy>>();
        
     // Adiconando lista de RNs para cada operacao de carirnho
        mapaCarrinho.put("SALVAR", rnsCarrinhoSalvar);
        
        regrasNegocio.put(Carrinho.class.getName(), mapaCarrinho);
        
     // ------------------------ Hash Pedido -------------------------------//
        
        Map<String, List<IStrategy>> mapaPedido = new HashMap<String, List<IStrategy>>();
        
        List<IStrategy> rnsPedidoSalvar = new ArrayList<IStrategy>();
        
        ValidarDataPedido validarDataPedido = new ValidarDataPedido(); 
        
        rnsPedidoSalvar.add(validarDataPedido);
        
        mapaPedido.put("SALVAR", rnsPedidoSalvar);
        
        regrasNegocio.put(Pedido.class.getName(), mapaPedido);
        
        // ---------------------- Fim Regras de negocio ----------------------//
    }


    @Override
    public Resultado salvar(EntidadeDominio entidade) {
    	
    	System.out.println("- Fachada Salvar");
        resultado = new Resultado();
        sb.setLength(0);
        String nmClasse = entidade.getClass().getName();

        Map<String, List<IStrategy>> mapaEntidade = regrasNegocio.get(nmClasse);
        List<IStrategy> rnsEntidade = mapaEntidade.get("SALVAR");

        executarRegras(entidade, rnsEntidade);

        if (sb.length() == 0) {
        	System.out.println("- Entrou no if de salvar fachada");
            IDAO dao = daos.get(nmClasse);
            
            resultado = dao.salvar(entidade);
        } else {
            resultado.add(entidade);
            resultado.setMsg(sb.toString());
        }
        return resultado;
    }

    @Override
    public Resultado alterar(EntidadeDominio entidade) {
    	System.out.println("- Fachada Alterar");
        resultado = new Resultado();
        sb.setLength(0);
        String nmClasse = entidade.getClass().getName();

        Map<String, List<IStrategy>> mapaEntidade = regrasNegocio.get(nmClasse);
        List<IStrategy> rnsEntidade = mapaEntidade.get("ALTERAR");

        executarRegras(entidade, rnsEntidade);

        if (sb.length() == 0) {
            IDAO dao = daos.get(nmClasse);
            dao.alterar(entidade);
            resultado.add(entidade);
        } else {
            resultado.add(entidade);
            resultado.setMsg(sb.toString());
        }

        return resultado;
    }

    @Override
    public Resultado excluir(EntidadeDominio entidade) {
    	System.out.println("- Fachada Excluir");
        resultado = new Resultado();
        String nmClasse = entidade.getClass().getName();

        IDAO dao = daos.get(nmClasse);
        resultado.add(entidade);
        dao.excluir(entidade);

        return resultado;
    }

    @Override
    public Resultado consultar(EntidadeDominio entidade) {
    	
    	System.out.println("- Fachada Consultar");
        resultado = new Resultado();
        String nmClasse = entidade.getClass().getName();

        IDAO dao = daos.get(nmClasse);
        resultado.setEntidades(dao.consultar(entidade).getEntidades());

        return resultado;

    }

    @Override
    public Resultado visualizar(EntidadeDominio entidade) {
        resultado = new Resultado();
        String nmClasse = entidade.getClass().getName();

        IDAO dao = daos.get(nmClasse);
        resultado.setEntidades(dao.consultar(entidade).getEntidades());
        return resultado;
    }
    

    private void executarRegras(EntidadeDominio entidade, List<IStrategy> rnsEntidade) {
        for (IStrategy rn : rnsEntidade) {
            String msg = rn.processar(entidade);
            if (msg != null) {
                sb.append(msg);
            }
        }
    }

}
