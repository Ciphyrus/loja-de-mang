package modelo.dominio;

import java.util.ArrayList;

public class Carrinho extends EntidadeDominio{
	
    private ArrayList<Manga> mangas = new ArrayList<Manga>();

    public Carrinho(ArrayList<Manga> mangas) {
        this.mangas = mangas;
    }
   
    public Carrinho(){
        
    }
    
    public ArrayList<Manga> getMangas() {
        return mangas;
    }

    public void setMangas(ArrayList<Manga> mangas) {
        this.mangas = mangas;
    }
    
    public void adicionarItem(Manga manga){
    	
    	if(mangas!=null) {
    		mangas.add(manga);    		
    	}else {
    		mangas = new ArrayList<Manga>();
    		mangas.add(manga);
    	}
    	
    }
        
}
