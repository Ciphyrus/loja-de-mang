package modelo.dominio;

import java.util.ArrayList;
import java.util.List;

public class Cliente extends EntidadeDominio {

    private String nomeCompleto;
    private String cpf;
    private String dataNascimento;
    private String email;
    private String ativo;
    private Senha senha;
    private double carteira;
    private List<Endereco> endereco = new ArrayList<Endereco>();
    private List<CartaoCredito> cartaoCredito = new ArrayList<CartaoCredito>();
    private int admin;

    
    // Enderecos e cartoes para criar cliente
    public Cliente(String nomeCompleto, String cpf, String dataNascimento, String email, Senha senha, String ativo, List<Endereco> endereco, List<CartaoCredito> cartaoCredito) {
        this.nomeCompleto = nomeCompleto;
        this.cpf = cpf;
        this.dataNascimento = dataNascimento;
        this.email = email;
        this.senha = senha;
        this.ativo = ativo;
        this.endereco = endereco;
        this.cartaoCredito = cartaoCredito;
    }
    
    // Constroi cliente sem o cartao e o endereco
    public Cliente(String nomeCompleto, String cpf, String dataNascimento, String email, Senha senha, String ativo, int admin) {
        this.dataNascimento = dataNascimento;
        this.email = email;
        this.ativo = ativo;
        this.cpf = cpf;
        this.nomeCompleto = nomeCompleto;
        this.senha = senha;
        this.admin = admin;
    }

    public Cliente() {
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAtivo() {
        return ativo;
    }

    public void setAtivo(String ativo) {
        this.ativo = ativo;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public Senha getSenha() {
        return senha;
    }
    
    public void setSenha(Senha senha) {
        this.senha = senha;
    }

    public List<Endereco> getEndereco() {
        return endereco;
    }

    public void setEndereco(List<Endereco> endereco) {
        this.endereco = endereco;
    }

    public List<CartaoCredito> getCartaoCredito() {
        return cartaoCredito;
    }

    public void setCartaoCredito(List<CartaoCredito> cartaoCredito) {
        this.cartaoCredito = cartaoCredito;
    }
    
    public int getAdmin() {
        return admin;
    }

    public void setAdmin(int admin) {
        this.admin = admin;
    }

    public double getCarteira() {
        return carteira;
    }

    public void setCarteira(double carteira) {
        this.carteira = carteira;
    } 
}
