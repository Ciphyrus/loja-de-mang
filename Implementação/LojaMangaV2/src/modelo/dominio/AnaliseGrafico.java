package modelo.dominio;

import java.util.ArrayList;
import java.util.Date;


public class AnaliseGrafico extends EntidadeDominio {
    
    private ArrayList<Manga> mangas;
    private Date dataInicio;
    private Date dataFim;
    private int tipoAnalise;
    private ArrayList<String> periodos;
    private ArrayList<String> dados;
    
    public AnaliseGrafico() {
    	
    }

    public ArrayList<Manga> getMangas() {
        return mangas;
    }

    public void setMangas(ArrayList<Manga> mangas) {
        this.mangas = mangas;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }
    
    public int getTipoAnalise() {
        return tipoAnalise;
    }

    public void setTipoAnalise(int tipoAnalise) {
        this.tipoAnalise = tipoAnalise;
    }

    public ArrayList<String> getPeriodos() {
        return periodos;
    }

    public void setPeriodos(ArrayList<String> periodos) {
        this.periodos = periodos;
    }

    public ArrayList<String> getDados() {
        return dados;
    }

    public void setDados(ArrayList<String> dados) {
        this.dados = dados;
    }
}
