
package modelo.dominio;

public class Estoque extends EntidadeDominio{
	
    private Manga manga;
    private int quantidadeEstoque;
    private String ativo;
    
    public Estoque() {
    	
    }

    public Estoque(Manga manga, int quantidadeEstoque) {
        this.manga = manga;
        this.quantidadeEstoque = quantidadeEstoque;
    }
    
    public Manga getManga() {
        return manga;
    }

    public void setManga(Manga manga) {
        this.manga = manga;
    }

    public int getQuantidadeEstoque() {
        return quantidadeEstoque;
    }

    public void setQuantidadeEstoque(int quantidadeEstoque) {
        this.quantidadeEstoque = quantidadeEstoque;
    }

    public String getAtivo() {
        return ativo;
    }

    public void setAtivo(String ativo) {
        this.ativo = ativo;
    }
    
    
}
