package modelo.dominio;

import java.util.ArrayList;

public class MangasPedido extends EntidadeDominio {
	
	private ArrayList<Manga> mangas = new ArrayList();

    public MangasPedido(ArrayList<Manga> mangas) {
        this.mangas = mangas;
    }
    
    public MangasPedido() {
        
    }
    
    public ArrayList<Manga> getMangas() {
        return mangas;
    }

    public void setMangas(ArrayList<Manga> mangas) {
        this.mangas = mangas;
    }
}
