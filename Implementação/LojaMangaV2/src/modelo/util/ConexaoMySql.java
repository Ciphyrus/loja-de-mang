
package modelo.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class ConexaoMySql {

    public static Connection getConecction() throws ClassNotFoundException, SQLException{
            
        
    	String url = "jdbc:mysql://localhost:3306/loja_manga?useTimezone=true&serverTimezone=UTC";
		String senha = "1234";
		String driver = "com.mysql.cj.jdbc.Driver";
		String user = "root";
		Connection conexao = null;
		Class.forName(driver);
		
		try {
			conexao = DriverManager.getConnection(url, user, senha);
			System.out.println("- ABRIU CONEXAO COM O BANCO DE DADOS!!!!");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return conexao;
		
	}
	
	public static void closeConnection(Connection conexao) {
		if(conexao != null) {
			try {
				conexao.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
