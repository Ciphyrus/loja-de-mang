package modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import modelo.dominio.Categoria;
import modelo.dominio.EntidadeDominio;
import modelo.dominio.Resultado;

public class CategoriaDAO extends AbstractDAO{
	
	public CategoriaDAO(Connection conexao, String table, String idTable) {
        super(conexao, "categoria", "cat_id");
    }

    public CategoriaDAO(String table, String idTable) {
        super("categoria", "cat_id");
    }

    public CategoriaDAO() {
        super("categoria", "cat_id");
    }

	@Override
	public Resultado salvar(EntidadeDominio entidadeDominio) {
		
		abrirConexao();
		
		Categoria categoria = (Categoria)entidadeDominio;
        
        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();

        sql.append("INSERT INTO " + nomeTabela + "(cat_nome)");
        sql.append(" VALUES (?)");	
        try {
            pst = conexao.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);

            pst.setString(1, categoria.getCategoria());
            
            pst.executeUpdate();		

            ResultSet rs = pst.getGeneratedKeys();
            int idCategoria = 0;
            if(rs.next()){
            	idCategoria = rs.getInt(1);
            }
            categoria.setId(idCategoria);
            
            conexao.commit();
            
        }catch (SQLException ex) {
        	ex.printStackTrace();
        	try {
        		conexao.rollback();
        	}catch(Exception e) {
        		e.printStackTrace();
        	}
        }finally {
        	try {
				conexao.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
        }
		return null;
	}

	@Override
	public Resultado alterar(EntidadeDominio entidadeDominio) {
		
		abrirConexao();
		
		Categoria categoria = (Categoria) entidadeDominio;
        
        
        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();

        sql.append("UPDATE " + nomeTabela + " SET (cat_nome = ?");
        sql.append(" WHERE " + idTabela + " = " + categoria.getId() + ";");
        try {
            pst = conexao.prepareStatement(sql.toString());

            pst.setString(1, categoria.getCategoria());
            
            pst.executeUpdate();		

            conexao.commit();				
        } catch (SQLException ex) {
        	ex.printStackTrace();
        	try {
        		conexao.rollback();
        	}catch(Exception e) {
        		e.printStackTrace();
        	}
        }finally {
        	try {
				conexao.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
        }
		return null;
	}

	@Override
	public Resultado consultar(EntidadeDominio entidadeDominio) {
		
		abrirConexao();
		
		// Incompleto
		
		Resultado resultado = null;
        return resultado;
	}

}
