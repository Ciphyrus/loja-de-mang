package modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import modelo.dominio.CartaoCredito;
import modelo.dominio.Cliente;
import modelo.dominio.Endereco;
import modelo.dominio.EntidadeDominio;
import modelo.dominio.Resultado;
import modelo.dominio.Senha;

public class ClienteDAO extends AbstractDAO {

	public ClienteDAO(Connection conexao, String table, String idTable) {
		super(conexao, "cliente", "cli_id");
	}

	public ClienteDAO(String table, String idTable) {
		super("cliente", "cli_id");
	}

	public ClienteDAO() {
		super("cliente", "cli_id");
	}

	@Override
	public Resultado salvar(EntidadeDominio entidadeDominio) {
		abrirConexao();

		Resultado resultado = new Resultado();

		PreparedStatement pst = null;
		Cliente cliente = (Cliente) entidadeDominio;
		StringBuilder sql = new StringBuilder();

		sql.append("INSERT INTO " + nomeTabela);
		sql.append(" (cli_nome,");
		sql.append(" cli_email,");
		sql.append(" cli_senha,");
		sql.append(" cli_data_nascimento,");
		sql.append(" ativo,");
		sql.append(" cli_carteira,");
		sql.append(" cli_admin,");
		sql.append(" cli_cpf");
		sql.append(") VALUES (?, ?, ?, ?, ?, ?, ?, ?)");

		try {
			pst = conexao.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);

			pst.setString(1, cliente.getNomeCompleto());
			pst.setString(2, cliente.getEmail());
			pst.setString(3, cliente.getSenha().getSenha());
			pst.setString(4, cliente.getDataNascimento());
			pst.setString(5, cliente.getAtivo());
			pst.setDouble(6, cliente.getCarteira());
			pst.setInt(7, cliente.getAdmin());
			pst.setString(8, cliente.getCpf());

			pst.executeUpdate();

			conexao.commit();

			resultado.add(cliente);
			System.out.println("- salvou");

		} catch (SQLException ex) {
			ex.printStackTrace();
			try {
				conexao.rollback();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} finally {
			try {
				conexao.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return resultado;
	}

	@Override
	public Resultado alterar(EntidadeDominio entidadeDominio) {

		abrirConexao();

		Resultado resultado = new Resultado();
		Cliente cliente = (Cliente) entidadeDominio;

		PreparedStatement pst = null;
		StringBuilder sql = new StringBuilder();

		sql.append("UPDATE " + nomeTabela + " SET ");
		sql.append("cli_nome=?, ");
		sql.append("cli_cpf=?, ");
		sql.append("cli_data_nascimento=?, ");
		sql.append("cli_email=?, ");
		sql.append("ativo = ? ");
		sql.append("WHERE " + idTabela + " = " + cliente.getId() + ";");

		try {

			pst = conexao.prepareStatement(sql.toString());
			pst.setString(1, cliente.getNomeCompleto());
			pst.setString(2, cliente.getCpf());
			pst.setString(3, cliente.getDataNascimento());
			pst.setString(4, cliente.getEmail());
			pst.setString(5, cliente.getAtivo());
			pst.executeUpdate();

		} catch (SQLException ex) {
			ex.printStackTrace();
			try {
				conexao.rollback();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} finally {
			try {
				conexao.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return resultado;

	}

	@Override
	public Resultado consultar(EntidadeDominio entidadeDominio) {

		abrirConexao();

		Resultado resultado = new Resultado();

		Cliente cliente = (Cliente) entidadeDominio;

		EnderecoDAO enderecosDAO = new EnderecoDAO();
		CartaoCreditoDAO cartoesDAO = new CartaoCreditoDAO();

		ResultSet rs;

		PreparedStatement pst = null;
		StringBuilder sql = new StringBuilder();

		// Login
		if (cliente.getNomeCompleto().equals("login") && cliente.getSenha() != null && cliente.getEmail() != null) {

			System.out.println("- IF de login");
			sql.append("SELECT * ");
			sql.append("FROM " + nomeTabela);
			sql.append(" WHERE cli_email = '" + cliente.getEmail() + "' AND cli_senha = '"
					+ cliente.getSenha().getSenha() + "';");

			// listar todos os clientes
		} else if (cliente.getId() == -1 && cliente.getNomeCompleto().equals("c")) {

			System.out.println("- IF de consultar");
			sql.append("SELECT * ");
			sql.append("FROM " + nomeTabela);

			// visualizar cliente
		} else if (cliente.getId() != 0 && cliente.getNomeCompleto().equals("v")) {

			System.out.println("- IF de visualizar");
			sql.append("SELECT * ");
			sql.append("FROM " + nomeTabela);
			sql.append(" WHERE " + idTabela + " = " + cliente.getId());

			// executado em validacao de email
		} else if (cliente.getNomeCompleto().equals("ValidaEmail")) {

			System.out.println("- IF de validar email");
			sql.append("SELECT * ");
			sql.append("FROM " + nomeTabela);
			sql.append(" WHERE cli_email = '" + cliente.getEmail() + "';");

		}

//        else if (cliente.getId() == 0 && !cliente.getNomeCompleto().equals("")) {
//        	
//            sql.append("SELECT * ");
//            sql.append("FROM " + table);
//            sql.append(" WHERE cli_nome like '%" + cliente.getNomeCompleto() + "%'");
//            
//        } 
		try {
			pst = conexao.prepareStatement(sql.toString());

			rs = pst.executeQuery();

			conexao.commit();

			cliente = null;

			while (rs.next()) {
				Senha senha = new Senha(rs.getString("cli_senha"));
				cliente = new Cliente(rs.getString("cli_nome"), rs.getString("cli_cpf"),
						rs.getString("cli_data_nascimento"), rs.getString("cli_email"), senha, rs.getString("ativo"),
						rs.getInt("cli_admin"));
				cliente.setCarteira(rs.getDouble("cli_carteira"));
				cliente.setId(rs.getInt("cli_id"));
				cliente.setAtivo(rs.getString("ativo"));

				System.out.println("\n\n\n\n- id de cliente no DAO " + cliente.getId());
				
				//vou consultar o endere�o via ID retornado
                List<Endereco> enderecos = new ArrayList<Endereco>();
                Endereco endereco = new Endereco();
                endereco.setCliente(cliente);
                
                for(EntidadeDominio e : enderecosDAO.consultarViaCliente(cliente).getEntidades()) {
                	Endereco end = (Endereco) e;
                	enderecos.add(end);
                }
                
                
                //vou consultar o cart�o via ID retornado
                ArrayList<CartaoCredito> cartoes = new ArrayList<CartaoCredito>();
                CartaoCredito cartao = new CartaoCredito();
                cartao.setCliente(cliente);
                
                for(EntidadeDominio e : cartoesDAO.consultar(cliente).getEntidades()) {
                	CartaoCredito crt = (CartaoCredito) e;
                	cartoes.add(crt);
                }
                

                // Salvando as consultas de endereco e cartao no cliente
                cliente.setEndereco(enderecos);
                cliente.setCartaoCredito(cartoes);
				resultado.add(cliente);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			try {
				conexao.rollback();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} finally {
			try {
				conexao.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return resultado;
	}
}
