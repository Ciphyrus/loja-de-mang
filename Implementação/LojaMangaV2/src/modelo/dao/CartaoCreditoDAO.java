package modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import modelo.dominio.CartaoCredito;
import modelo.dominio.Cliente;
import modelo.dominio.EntidadeDominio;
import modelo.dominio.Manga;
import modelo.dominio.Resultado;

public class CartaoCreditoDAO extends AbstractDAO {

    public CartaoCreditoDAO(Connection conexao, String nomeTabela, String idTabela) {
        super(conexao, "cartao_de_credito", "ctcrd_id");
    }

    public CartaoCreditoDAO(String nomeTabela, String idTabela) {
        super("cartao_de_credito", "ctcrd_id");
    }

    public CartaoCreditoDAO() {
        super("cartao_de_credito", "ctcrd_id");
    }

    @Override
    public Resultado salvar(EntidadeDominio entidadeDominio) {
    	
    	System.out.println("Entrou no salvar do ccDAO");
    	
    	abrirConexao();
    	
    	Resultado resultado = new Resultado();
    	
        CartaoCredito cc = (CartaoCredito) entidadeDominio;
        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();

        sql.append("INSERT INTO " + nomeTabela + " (");
        
        sql.append("ctcrd_numero_cartao, ");
        sql.append("ctcrd_bandeira, ");
        sql.append("ctcrd_nome_impresso, ");
        sql.append("ctcrd_validade, ");
        sql.append("ctcrd_codigo_seguranca, ");
        sql.append("FK_cli_id_ctcrd");
        
        sql.append(") VALUES (?, ?, ?, ?, ?, ?)");
        try {
            pst = conexao.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);

            pst.setString(1, cc.getNumeroCartao());
            pst.setString(2, cc.getBandeira());
            pst.setString(3, cc.getNomeImpresso());
            pst.setString(4, cc.getValidade());
            pst.setString(5, cc.getCodigoSeguranca());
            pst.setInt   (6, cc.getCliente().getId());
            
            System.out.println("PST Cartao: " + pst);
            
            pst.executeUpdate();
            conexao.commit();

            ResultSet rs = pst.getGeneratedKeys();
            int idCartaoCredito = 0;
            if (rs.next()) {
                idCartaoCredito = rs.getInt(1);
            }
            cc.setId(idCartaoCredito);

            resultado.add(cc);
            conexao.close();
        } catch (SQLException ex) {
        	ex.printStackTrace();
        	try {
        		conexao.rollback();
        	}catch(Exception e) {
        		e.printStackTrace();
        	}
        }finally {
        	try {
				conexao.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
        }
		return resultado;
    }

    // Recebe um Cliente e atualiza todos os cartoes que o Cliente possui.
    @Override
    public Resultado alterar(EntidadeDominio entidadeDominio) {
    	
    	abrirConexao();
    	
    	Resultado resultado = new Resultado();
    	
        CartaoCredito cartao;
        try {
        	cartao = (CartaoCredito) entidadeDominio;
        } catch (ClassCastException e) {
        	cartao = new CartaoCredito();
        	cartao.setAtivo("ATIVO");
        }

        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();

        sql.append("UPDATE " + nomeTabela + " SET ");
        
        sql.append("ctcrd_numero_cartao = ?, ");
        sql.append("ctcrd_bandeira = ?, ");
        sql.append("ctcrd_nome_impresso = ?, ");
        sql.append("ctcrd_validade = ?, ");
        sql.append("ctcrd_codigo_seguranca = ?");
        
        sql.append(" WHERE (" + idTabela + " = " + cartao.getId() + ");");
        
        try {
            pst = conexao.prepareStatement(sql.toString());

            pst.setString(1, cartao.getNumeroCartao());
            pst.setString(2, cartao.getBandeira());
            pst.setString(3, cartao.getNomeImpresso());
            pst.setString(4, cartao.getValidade());
            pst.setString(5, cartao.getCodigoSeguranca());
            
            System.out.println("CartaoCreditoDAO: pst alterar = " + pst);
            
            pst.executeUpdate();
            conexao.commit();
            
            System.out.println("- Alterou");
        } catch (SQLException ex) {
        	ex.printStackTrace();
        	try {
        		conexao.rollback();
        	}catch(Exception e) {
        		e.printStackTrace();
        	}
        }finally {
        	try {
				conexao.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
        }
		return resultado;
    }

    // Consulta um cartai com base no ID de cartao recebido por parametro
    @Override
    public Resultado consultar(EntidadeDominio entidadeDominio) {
    	
    	abrirConexao();
    	Cliente cliente = (Cliente) entidadeDominio;
    	CartaoCredito cartao = new CartaoCredito();
    	
        Resultado resultado = new Resultado();
       
         
        ResultSet rs;
        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();

        if(cartao.getId() == -1) {
        	
        	sql.append("SELECT * ");
            sql.append("FROM " + nomeTabela);
            
        } else {
        	
            sql.append("SELECT * ");
            sql.append("FROM " + nomeTabela);
            sql.append(" WHERE FK_cli_id_ctcrd = " + cliente.getId());
            
        }

        try {
            pst = conexao.prepareStatement(sql.toString());

            rs = pst.executeQuery();
            
            conexao.commit();
            cartao = null;

            while (rs.next()) {        
                cartao = new CartaoCredito(rs.getString("ctcrd_numero_cartao"), rs.getString("ctcrd_bandeira"),
                						   rs.getString("ctcrd_nome_impresso"),rs.getString("ctcrd_validade"),
                						   rs.getString("ctcrd_codigo_seguranca"));
                cliente.setId(rs.getInt("FK_cli_id_ctcrd"));
                cartao.setCliente(cliente);
                
                cartao.setId(rs.getInt("ctcrd_id"));
                
                System.out.println("ccDAO: validade(get/rs): " + cartao.getValidade() + "/ " + rs.getString("ctcrd_validade"));
                
                resultado.add(cartao);
            }
        } catch (SQLException ex) {
        	ex.printStackTrace();
        	try {
        		conexao.rollback();
        	}catch(Exception e) {
        		e.printStackTrace();
        	}
        }finally {
        	try {
				conexao.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
        }
        return resultado;
    }
    
public Resultado consultarViaCliente(EntidadeDominio entidadeDominio) {
    	
    	abrirConexao();
    	
        Cliente cli = (Cliente) entidadeDominio;
        Resultado resultado = new Resultado();
         
        CartaoCredito cartao = new CartaoCredito();
        ResultSet rs;
        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();

    	sql.append("SELECT * ");
        sql.append("FROM " + nomeTabela);
        sql.append(" WHERE FK_cli_id_ctcrd = " + cli.getId());

        try {
            pst = conexao.prepareStatement(sql.toString());

            rs = pst.executeQuery();
            
            conexao.commit();
           

            while (rs.next()) {
            	
            	Cliente cliente = new Cliente();   
                cartao = new CartaoCredito(	rs.getString("ctcrd_numero_cartao"), rs.getString("ctcrd_bandeira"),
                							rs.getString("ctcrd_nome_impresso"),rs.getString("ctcrd_validade"),
                							rs.getString("ctcrd_codigo_seguranca"));
                
                //FAZER A CONSULTA DE CART�O FUNCIONAR
                
                cliente.setId(rs.getInt("FK_cli_id_ctcrd"));
                cartao.setCliente(cliente);
                
                cartao.setId(rs.getInt("ctcrd_id"));
                
                resultado.add(cartao);
            }
        } catch (SQLException ex) {
        	ex.printStackTrace();
        	try {
        		conexao.rollback();
        	}catch(Exception e) {
        		e.printStackTrace();
        	}
        }finally {
        	try {
				conexao.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
        }
        return resultado;
    }

public Resultado consultarViaId(EntidadeDominio entidadeDominio) {
	
	abrirConexao();
	
	Cliente cli = (Cliente) entidadeDominio;
	CartaoCredito cartao = null;
    Resultado resultado = new Resultado();
    ResultSet rs;
    PreparedStatement pst = null;
    StringBuilder sql = new StringBuilder();

	sql.append("SELECT * ");
    sql.append("FROM " + nomeTabela);
    sql.append(" WHERE FK_cli_id_ctcrd = " + cli.getId());

    try {
        pst = conexao.prepareStatement(sql.toString());

        rs = pst.executeQuery();
        
        conexao.commit();       

        while (rs.next()) {
        	
        	Cliente cliente = new Cliente();   
            cartao = new CartaoCredito(	rs.getString("ctcrd_numero_cartao"), rs.getString("ctcrd_bandeira"),
            							rs.getString("ctcrd_nome_impresso"),rs.getString("ctcrd_validade"),
            							rs.getString("ctcrd_codigo_seguranca"));
            
            cliente.setId(rs.getInt("FK_cli_id_ctcrd"));
            cartao.setCliente(cliente);
            
            cartao.setId(rs.getInt("ctcrd_id"));
            
            resultado.add(cartao);
        }
    } catch (SQLException ex) {
    	ex.printStackTrace();
    	try {
    		conexao.rollback();
    	}catch(Exception e) {
    		e.printStackTrace();
    	}
    }finally {
    	try {
			conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
    return resultado;
}


}
