package modelo.dao;



import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;

import modelo.dominio.EntidadeDominio;
import modelo.dominio.Resultado;
import modelo.util.ConexaoMySql;


public abstract class AbstractDAO implements IDAO {
	
	protected String idTabela;
	protected String nomeTabela;
	protected Connection conexao = null;
	protected boolean controleTransicao = true;

    
public AbstractDAO(Connection conexao, String nomeTabela, String idTabela) {
		
		this.conexao = conexao;
		this.idTabela = idTabela;
		this.nomeTabela = nomeTabela;
	}
	
	public AbstractDAO(String nomeTabela, String idTabela) {
		
		this.nomeTabela = nomeTabela;
		this.idTabela = idTabela;
	}

public void abrirConexao() {
		
		try {
			
			if(conexao == null || conexao.isClosed()) {
				System.out.println("- entrou no IF do m�todo abrirConexao()");
				conexao = ConexaoMySql.getConecction();
				System.out.println("- Conseguiu abrir conex�o");
				conexao.setAutoCommit(false);
				System.out.println("- settou auto commit como False");
			}
			System.out.println("- conexao aberta");
			
		}catch(SQLException|ClassNotFoundException e) {
			System.out.println("- erro ao abrir conexao com banco de dados");
			e.printStackTrace();
		}
		
	}
	

	@Override
	public Resultado excluir(EntidadeDominio ent) {
		
		abrirConexao();
		
		PreparedStatement pst = null;
		StringBuilder sql = new StringBuilder();
		Resultado resultado = new Resultado();
		
		
		sql.append("UPDATE "+ nomeTabela +" SET ativo = 'INATIVO' WHERE ("+ idTabela +" = " + ent.getId() + ");");
		
		try {
			pst = conexao.prepareStatement(sql.toString());
			
			pst.executeUpdate();
			
			conexao.commit();
			
			resultado.add(ent);
			System.out.println("- exclu�do com sucesso");
		}catch (SQLException e) {
			try {
				conexao.rollback();
			}catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			try {
				conexao.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return resultado;
	}
	
	public Resultado reativar(EntidadeDominio ent) {
		abrirConexao();
		
		PreparedStatement pst = null;
		StringBuilder sql = new StringBuilder();
		Resultado resultado = new Resultado();
		
		
		sql.append("UPDATE "+ nomeTabela +" SET ativo = 'INATIVO' WHERE (id = " + ent.getId() + ");");
		
		try {
			pst = conexao.prepareStatement(sql.toString());
			
			pst.executeUpdate();
			
			conexao.commit();
			
			resultado.add(ent);
			System.out.println("- Reativado com sucesso com sucesso");
		}catch (SQLException e) {
			try {
				conexao.rollback();
			}catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			try {
				conexao.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return resultado;
	}
}
