package view.viewHelper;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelo.dao.EnderecoDAO;
import modelo.dominio.Cliente;
import modelo.dominio.Endereco;
import modelo.dominio.EntidadeDominio;
import modelo.dominio.Resultado;

public class EnderecoVH implements IViewHelper {
	
	public EntidadeDominio getEntidade (HttpServletRequest request) {
		
		HttpSession session = null;
		Endereco end = new Endereco();

		if (request.getParameter("idEnd") != null && !request.getParameter("idEnd").trim().equals("")) {
			end.setId(Integer.parseInt(request.getParameter("idEnd")));
		} else {
			end.setId(0);
		}

		String operacao = request.getParameter("OPERACAO");

		if (operacao.equals("ALTERAR")) {

			end = buildEnderecoAlterar(request);

		} else if (operacao.equals("SALVAR")) {

			session = request.getSession();
			end = buildEnderecoSalvar(request);

		}

		return end;
	}

	@Override
	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		
		Endereco end = new Endereco();
		RequestDispatcher rd = null;

		try {
			end = (Endereco) resultado.getEntidades().get(0);
		} catch (Exception e) {
			end = new Endereco();
		}

		String operacao = request.getParameter("OPERACAO");
		
		if (request.getParameter("idEnd") != null && !request.getParameter("idEnd").trim().equals("")) {

			end.setId(Integer.parseInt(request.getParameter("idEnd")));

		} else {
			end.setId(0);
		}
		
		if (operacao.equals("ALTERAR")) {

			EnderecoDAO endDao = new EnderecoDAO();
			end = (Endereco) resultado.getEntidades().get(0);
			request.getSession(true).setAttribute("AlterarEnd", end);
			
			System.out.println("EndVH: End ID: " + end.getId());

			// end.setId(-1);
			resultado = endDao.consultar(end);

			request.getSession().setAttribute("resultado", resultado);

			System.out.println("- Redirecionando para p�gina alterar Endereco");

			rd = request.getRequestDispatcher("autenticado/edit-address.jsp");
		} else if (operacao.equals("SALVAR")) {

			request.getSession().setAttribute("resultado", resultado);
			request.getSession().setAttribute("endereco", end);
			rd = request.getRequestDispatcher("autenticado/customer-profile.jsp");

		}
		
		rd.forward(request, response);
	}
	
	private Endereco buildEnderecoAlterar(HttpServletRequest request) {
	
		Endereco endereco = new Endereco();
	
		// Criando Endereco
		String logradouro = request.getParameter("txtLogradouro");
		String numeroEndereco = request.getParameter("txtNumero");
		String cep = request.getParameter("txtCep");
		String complemento = request.getParameter("txtComplemento");
		String bairro = request.getParameter("txtBairro");
		String cidade = request.getParameter("txtCidade");
		String siglaEstado = request.getParameter("selectEstado");
		String tipoEnd = request.getParameter("selectTipoEnd");
		String ativo = "ATIVO";
		
		Cliente cli = (Cliente) request.getSession().getAttribute("clienteLogado");
		
		// Verificando compos nulos e atribuindo valores
		int id = Integer.valueOf(request.getParameter("idEnd"));
	
		if (logradouro != null) {
			endereco.setLogradouro(logradouro);
		} else {
			endereco.setLogradouro("");
		}
		
		if (numeroEndereco != null) {
			endereco.setNumero(numeroEndereco);
		} else {
			endereco.setNumero("");
		}
		
		if (cep != null) {
			endereco.setCep(cep);
		} else {
			endereco.setCep("");
		}
		
		if (complemento != null) {
			endereco.setComplemento(complemento);
		} else {
			endereco.setComplemento("");
		}
		
		if (bairro != null) {
			endereco.setBairro(bairro);
		} else {
			endereco.setBairro("");
		}
		
		if (cidade != null) {
			endereco.setCidade(cidade);
		} else {
			endereco.setCidade("");
		}
		
		if (siglaEstado != null) {
			endereco.setEstado(siglaEstado);
		} else {
			endereco.setEstado("");
		}
		
		if (tipoEnd != null) {
			endereco.setTipo(tipoEnd);
		} else {
			endereco.setTipo("");
		}
	
		endereco.setCliente(cli);
		endereco.setAtivo(ativo);
	
		endereco.setId(id);
		System.out.println("- Entidade Endereco montada com sucesso no VH");
	
		return endereco;
	}

	private Endereco buildEnderecoSalvar(HttpServletRequest request) {

		Endereco endereco = new Endereco();

		// Criando Endereco
				String logradouro = request.getParameter("txtLogradouro");
				String numeroEndereco = request.getParameter("txtNumero");
				String cep = request.getParameter("txtCep");
				String complemento = request.getParameter("txtComplemento");
				String bairro = request.getParameter("txtBairro");
				String cidade = request.getParameter("txtCidade");
				String estado = request.getParameter("selectEstado");
				String tipoEnd = request.getParameter("selectTipoEnd");
				String ativo = "ATIVO";
				
				Cliente cli = (Cliente) request.getSession().getAttribute("clienteLogado");
				
				System.out.println("Nome cliente: " + cli.getNomeCompleto());
				System.out.println("ID cliente: " + cli.getId());

				// Verificando compos nulos e atribuindo valores

				if (logradouro != null) {
					endereco.setLogradouro(logradouro);
				} else {
					endereco.setLogradouro("");
				}
				
				if (numeroEndereco != null) {
					endereco.setNumero(numeroEndereco);
				} else {
					endereco.setNumero("");
				}
				
				if (cep != null) {
					endereco.setCep(cep);
				} else {
					endereco.setCep("");
				}
				
				if (complemento != null) {
					endereco.setComplemento(complemento);
				} else {
					endereco.setComplemento("");
				}
				
				if (bairro != null) {
					endereco.setBairro(bairro);
				} else {
					endereco.setBairro("");
				}
				
				if (cidade != null) {
					endereco.setCidade(cidade);
				} else {
					endereco.setCidade("");
				}
				
				if (estado != null) {
					endereco.setEstado(estado);
				} else {
					endereco.setEstado("");
				}
				
				if (tipoEnd != null) {
					endereco.setTipo(tipoEnd);
				} else {
					endereco.setTipo("");
				}

				endereco.setCliente(cli);
				endereco.setAtivo(ativo);

		System.out.println("- Entidade Endere�o montada com sucesso no VH");
		return endereco;
	}
}
