package view.viewHelper;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelo.dominio.Carrinho;
import modelo.dominio.EntidadeDominio;
import modelo.dominio.Manga;
import modelo.dominio.Resultado;

public class CarrinhoVH implements IViewHelper {

	@Override
	public EntidadeDominio getEntidade(HttpServletRequest request) {
		
		HttpSession session = null;
		Carrinho carrinho = null;
		String operacao = request.getParameter("OPERACAO");

		if(request.getAttribute("carrinho") != null) {
			carrinho = (Carrinho)request.getAttribute("carrinho");
		}else {
			carrinho = new Carrinho();
		}
		
		
		
		if(operacao.trim().equals("SALVAR")) {
			
			Manga mg = (Manga) request.getSession().getAttribute("VisualizarMg");
			int quantidade = Integer.valueOf(request.getParameter("txtQntde"));
			
			
			if(quantidade != 0) {
				mg.setQuantidadeProdutoCarrinho(quantidade);
				carrinho.adicionarItem(mg);
			} else {
				mg.setQuantidadeProdutoCarrinho(1);
				carrinho.adicionarItem(mg);
			}

			System.out.println("- QUANTIDADE DE PRODUTO NO CARRINHO: " + carrinho.getMangas().get(0).getQuantidadeProdutoCarrinho() + " do T�tulo: " +  carrinho.getMangas().get(0).getTitulo());
		}
		
		
		return carrinho;
	}

	@Override
	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		
		HttpSession session = null;
		String operacao = request.getParameter("OPERACAO");
		RequestDispatcher rd = null;
		
		Carrinho carrinho = (Carrinho) resultado.getEntidades().get(0);
		
		if(operacao.trim().equals("SALVAR")) {
			request.getSession().setAttribute("carrinho", carrinho);
			
			if(request.getSession().getAttribute("clienteLogado") != null) {
				rd = request.getRequestDispatcher("autenticado/cart.jsp");
			}else {
				rd = request.getRequestDispatcher("login.jsp");
			}
			
		}
		
		rd.forward(request, response);
	}

}
