package view.viewHelper;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelo.dao.CartaoCreditoDAO;
import modelo.dominio.CartaoCredito;
import modelo.dominio.Cliente;
import modelo.dominio.EntidadeDominio;
import modelo.dominio.Resultado;

public class CartaoCreditoVH implements IViewHelper {

	@Override
	public EntidadeDominio getEntidade(HttpServletRequest request) {

		HttpSession session = null;
		CartaoCredito cartao = new CartaoCredito();

		if (request.getParameter("idCartao") != null && !request.getParameter("idCartao").trim().equals("")) {

			cartao.setId(Integer.parseInt(request.getParameter("idCartao")));

		} else {
			cartao.setId(0);
		}

		String operacao = request.getParameter("OPERACAO");

		if (operacao.equals("CONSULTAR")) {

			cartao.setId(-1);

			session = request.getSession();
			request.setAttribute("cartao", cartao);

		} else if (operacao.equals("SALVAR")) {

			session = request.getSession();
			cartao = buildCartaoSalvar(request);

		} else if (operacao.equals("ALTERAR")) {
			
			System.out.println("- VH Alterar Cartao");

			cartao = buildCartaoAlterar(request);

		} else if (operacao.equals("EXCLUIR")) {

			session = request.getSession();
			cartao = new CartaoCredito();
			cartao.setId(Integer.parseInt(request.getParameter("idCartao")));

		} else if (operacao.equals("VISUALIZAR")) {

			session = request.getSession();
			cartao.setId(Integer.valueOf(request.getParameter("idCartao")));
			System.out.println("id de cartao para visualizar "+cartao.getId());
			cartao.setNomeImpresso("");
			request.setAttribute("VisualizarCartao", cartao);

		}

		return cartao;
	}

	@Override
	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		CartaoCredito cartao = new CartaoCredito();
		RequestDispatcher rd = null;

		try {
			cartao = (CartaoCredito) resultado.getEntidades().get(0);
		} catch (Exception e) {
			cartao = new CartaoCredito();
		}

		String operacao = request.getParameter("OPERACAO");

		if (request.getParameter("idCartao") != null && !request.getParameter("idCartao").trim().equals("")) {

			cartao.setId(Integer.parseInt(request.getParameter("idCartao")));

		} else {
			cartao.setId(0);
		}

		if (operacao.equals("CONSULTAR")) {

			cartao = (CartaoCredito) resultado.getEntidades().get(0);
			request.getSession().setAttribute("resultado", resultado);
			request.getSession().setAttribute("cartao", cartao);
			rd = request.getRequestDispatcher("autenticado/customer-profile.jsp");

		} else if (operacao.equals("SALVAR")) {

			Cliente cliente = (Cliente) request.getSession().getAttribute("clienteLogado");
			CartaoCreditoDAO cartaoDao = new CartaoCreditoDAO();
			resultado = cartaoDao.consultar(cliente);
			request.getSession().setAttribute("resultado", resultado);
			request.getSession().setAttribute("cartao", cartao);
			rd = request.getRequestDispatcher("autenticado/customer-profile.jsp");

		}
		if (operacao.equals("VISUALIZAR")) {

			cartao = (CartaoCredito) resultado.getEntidades().get(0);
			request.getSession(true).setAttribute("VisualizarCartao", cartao);

			System.out.println("- Redirecionando para p�gina visualizar");
			
			rd = request.getRequestDispatcher("autenticado/customer-profile.jsp");
		}
		if (operacao.equals("ALTERAR")) {

			CartaoCreditoDAO cartaoDao = new CartaoCreditoDAO();
			cartao = (CartaoCredito) resultado.getEntidades().get(0);
			request.getSession(true).setAttribute("AlterarCartao", cartao);
			
			System.out.println("ID cart�o no ccVH setview: " + cartao.getId());
			System.out.println("Numero cart�o no ccVH setview: " + cartao.getNumeroCartao());			

			//cartao.setId(-1);
			resultado = cartaoDao.consultar(cartao);

			request.getSession().setAttribute("resultado", resultado);

			System.out.println("- Redirecionando para p�gina alterar");

			rd = request.getRequestDispatcher("autenticado/edit-credit-card.jsp");
		}
		if (operacao.equals("EXCLUIR")) {

			rd = request.getRequestDispatcher("autenticado/customer-profile.jsp");

		}

		rd.forward(request, response);
	}
	
	private CartaoCredito buildCartaoSalvar(HttpServletRequest request) {

		CartaoCredito cartao = new CartaoCredito();

		// Criando Cartao
		String numeroCartao = request.getParameter("txtNumeroCartao");
	    String bandeira = request.getParameter("txtBandeira");
	    String nomeImpresso = request.getParameter("txtNomeImpresso");
	    String validade = request.getParameter("txtValidade");
	    String codigoSeguranca = request.getParameter("txtCodigoSeguranca");
	    String ativo = "ATIVO";
	    
	    Cliente cli = (Cliente) request.getSession().getAttribute("clienteLogado");

		// Atribuindo dados de Cartao
		if (numeroCartao != null) {
			cartao.setNumeroCartao(numeroCartao);
		} else {
			cartao.setNumeroCartao("");
		}
		if (bandeira != null) {
			cartao.setBandeira(bandeira);
		} else {
			cartao.setBandeira("");
		}
		if (nomeImpresso != null) {
			cartao.setNomeImpresso(nomeImpresso);
		} else {
			cartao.setNomeImpresso("");
		}
		if (validade != null) {
			cartao.setValidade(validade);
		} else {
			cartao.setValidade("");
		}
		if (codigoSeguranca != null) {
			cartao.setCodigoSeguranca(codigoSeguranca);
		} else {
			cartao.setCodigoSeguranca("");
		}
		
		//Atribuindo Cliente
		cartao.setCliente(cli);

		// Atribuindo Status a Mang�
		cartao.setAtivo(ativo);

		System.out.println("- Entidade Cartao montada com sucesso no VH");
		return cartao;
	}
	
	private CartaoCredito buildCartaoAlterar(HttpServletRequest request) {

		CartaoCredito cartao = new CartaoCredito();

		// Criando Cartao
			System.out.println("Request txtNumeroCartao: " + request.getParameter("txtNumeroCartao"));
			
			String numeroCartao = request.getParameter("txtNumeroCartao");
		    String bandeira = request.getParameter("txtBandeira");
		    String nomeImpresso = request.getParameter("txtNomeImpresso");
		    String validade = request.getParameter("txtValidade");
		    String codigoSeguranca = request.getParameter("txtCodigoSeguranca");
		    Cliente cli = (Cliente) request.getSession().getAttribute("clienteLogado");
			    String ativo = "ATIVO";

		int id = Integer.valueOf(request.getParameter("idCartao"));

		// Atribuindo dados de Cartao
				if (numeroCartao != null) {
					cartao.setNumeroCartao(numeroCartao);
				} else {
					cartao.setNumeroCartao("");
				}
				if (bandeira != null) {
					cartao.setBandeira(bandeira);
				} else {
					cartao.setBandeira("");
				}
				if (nomeImpresso != null) {
					cartao.setNomeImpresso(nomeImpresso);
				} else {
					cartao.setNomeImpresso("");
				}
				if (validade != null) {
					cartao.setValidade(validade);
				} else {
					cartao.setValidade("");
				}
				if (codigoSeguranca != null) {
					cartao.setCodigoSeguranca(codigoSeguranca);
				} else {
					cartao.setCodigoSeguranca("");
				}
				
				System.out.println("ccVH: Validade(get/String): " + cartao.getValidade() + "/ " + validade);
				
				// Cliente do Cartao		
				cartao.setCliente(cli);

				// Atribuindo Status a Mang�
				cartao.setAtivo(ativo);
				
				// Atribuindo ID
				cartao.setId(id);
				
				System.out.println("\tInfos do cartao no buildAlterar:");
			    System.out.println("Numero: " + numeroCartao);
			    System.out.println("Bandeira: " + bandeira);
			    System.out.println("Nome: " + nomeImpresso);
			    System.out.println("Validade: " + validade);
			    System.out.println("C�digo: " + codigoSeguranca);
				
		System.out.println("- Entidade Cartao montada com sucesso no VH");

		return cartao;
	}

}
