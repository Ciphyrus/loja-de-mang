package view.viewHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelo.dao.ClienteDAO;
import modelo.dominio.Cliente;
import modelo.dominio.Endereco;
import modelo.dominio.EntidadeDominio;
import modelo.dominio.Resultado;
import modelo.dominio.Senha;

public class ClienteVH implements IViewHelper {

	@Override
	public EntidadeDominio getEntidade(HttpServletRequest request) {

		HttpSession session = null;
		Cliente cli = new Cliente();

		if (request.getParameter("idCli") != null && !request.getParameter("idCli").trim().equals("")) {

			cli.setId(Integer.parseInt(request.getParameter("idCli")));

		} else {

		}

		String operacao = request.getParameter("OPERACAO");

		if (operacao.equals("CONSULTAR")) {

			cli.setId(-1);
			cli.setNomeCompleto("c");

			session = request.getSession();
			request.setAttribute("cliente", cli);

		} else if (operacao.equals("SALVAR")) {

			session = request.getSession();
			cli = buildClienteSalvar(request);

		} else if (operacao.equals("ALTERAR")) {

			cli = buildClienteAlterar(request);

		} else if (operacao.equals("EXCLUIR")) {

			session = request.getSession();
			cli = new Cliente();
			cli.setId(Integer.parseInt(request.getParameter("idCli")));

		} else if (operacao.equals("VISUALIZAR")) {

			session = request.getSession();
			cli.setId(Integer.valueOf(request.getParameter("idCli")));
			System.out.println(cli.getId());
			cli.setNomeCompleto("v");
			request.setAttribute("VisualizarCli", cli);

		} else if (operacao.equals("LOGIN")) {

			cli.setNomeCompleto("login");
			Senha senha = new Senha();

			if (request.getParameter("txtSenha") != null) {
				senha.setSenha(request.getParameter("txtSenha"));
				cli.setSenha(senha);
			} else {
				senha.setSenha("");
				cli.setSenha(senha);
			}

			if (request.getParameter("txtEmail") != null) {
				cli.setEmail(request.getParameter("txtEmail"));
			} else {
				cli.setEmail("");
			}
		}

		return cli;
	}

	@Override
	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		Cliente cli = new Cliente();
		RequestDispatcher rd = null;

		try {
			cli = (Cliente) resultado.getEntidades().get(0);
		} catch (Exception e) {
			cli = new Cliente();
		}
		
		System.out.println("ID do cliente que entrou no setView: " + cli.getId());

		String operacao = request.getParameter("OPERACAO");

		/*if (request.getParameter("idCli") != null && !request.getParameter("idCli").trim().equals("")) {

			cli.setId(Integer.parseInt(request.getParameter("idCli")));

		} else {
			cli.setId(0);
		}*/
		
		System.out.println("ID do cliente que entrou no setView ap�s mudan�a de ID: " + cli.getId());

		if (operacao.equals("CONSULTAR")) {

			cli = (Cliente) resultado.getEntidades().get(0);
			request.getSession().setAttribute("resultado", resultado);
			request.getSession().setAttribute("cliente", cli);

			rd = request.getRequestDispatcher("autenticado/admin/customer-list.jsp");

		} else if (operacao.equals("SALVAR")) {

			rd = request.getRequestDispatcher("customer-list.jsp");

		} else if (operacao.equals("VISUALIZAR")) {

			cli = (Cliente) resultado.getEntidades().get(0);
			System.out.println("id de cliente na VH: " + cli.getId());
			request.getSession(true).setAttribute("VisualizarCli", cli);

			System.out.println("- Redirecionando para p�gina");
			rd = request.getRequestDispatcher("autenticado/admin/customerAdminViewProfile.jsp");
		} else if (operacao.equals("EXCLUIR")) {

			ClienteDAO cliDao = new ClienteDAO();
			cli = new Cliente();
			cli.setId(-1);
			cli.setNomeCompleto("c");

			resultado = cliDao.consultar(cli);

			request.getSession().setAttribute("resultado", resultado);

			rd = request.getRequestDispatcher("autenticado/admin/customer-list.jsp");

		} else if (operacao.equals("ALTERAR")) {

			rd = request.getRequestDispatcher("");

		} else if (operacao.equals("LOGIN")) {

			if (resultado.getEntidades() != null && resultado.getEntidades().get(0) != null) {
				cli = (Cliente) resultado.getEntidades().get(0);

				if (cli.getAtivo().trim().equals("INATIVO")) {

					resultado.setMsg("Conta desativada.");
					request.getSession().setAttribute("resultado", resultado);
					rd = request.getRequestDispatcher("login.jsp");
				} else if (cli.getAdmin() == 1) {
					rd = request.getRequestDispatcher("autenticado/admin/admin-profile.jsp");
					request.getSession().setAttribute("resultado", resultado);
					request.getSession().setAttribute("clienteLogado", cli);
				} else {
					rd = request.getRequestDispatcher("autenticado/customer-profile.jsp");
					request.getSession().setAttribute("resultado", resultado);
					request.getSession().setAttribute("clienteLogado", cli);
					
					System.out.println("ID do cliente que logou: " + cli.getId());
				}
			} else {
				resultado.setMsg("Login ou senha incorretos.");

				request.getSession().setAttribute("resultado", resultado);
				rd = request.getRequestDispatcher("login.jsp");
			}
		}

		rd.forward(request, response);
	}

	private Cliente buildClienteSalvar(HttpServletRequest request) {

		Cliente cliente = new Cliente();
		// Criando Cliente
		String nomeCompleto = request.getParameter("txtNomeCompleto");
		String cpf = request.getParameter("txtCpf");
		String dataNascimento = request.getParameter("dateDataNascimento");
		String email = request.getParameter("txtEmail");
		String senhaConfirma = request.getParameter("txtConfirmaSenha");
		String senha = request.getParameter("txtSenha");
		String ativo = "ATIVO";

		Senha senhaCliente = new Senha();

		

		// Atribuindo dados de Cliente
		if (dataNascimento != null) {
			cliente.setDataNascimento(dataNascimento);
		} else {
			cliente.setDataNascimento("");
		}

		if (!nomeCompleto.trim().equals("") || nomeCompleto != null) {
			cliente.setNomeCompleto(nomeCompleto);
		} else {
			cliente.setNomeCompleto("");
		}

		if (!cpf.trim().equals("") || cpf != null) {
			cliente.setCpf(cpf);
		} else {
			cliente.setCpf("");
		}

		if (!email.trim().equals("") || email != null) {
			cliente.setEmail(email);
		} else {
			cliente.setEmail("");
		}

		if (!senhaConfirma.equals("") || senhaConfirma != null) {
			senhaCliente.setConfirmaSenha(senhaConfirma);
		} else {
			senhaCliente.setConfirmaSenha("");
		}

		if (!senha.trim().equals("") || senha != null) {
			senhaCliente.setSenha(senha);
		} else {
			senhaCliente.setSenha("");
		}

		cliente.setSenha(senhaCliente);

		cliente.setAtivo(ativo);

		cliente.setCarteira(0);

		System.out.println("- Entidade Cliente montada com sucesso no VH");
		return cliente;
	}

	private Cliente buildClienteAlterar(HttpServletRequest request) {

		Cliente cliente = new Cliente();

		// Criando Cliente
		String nomeCompleto = request.getParameter("txtNomeCompleto");
		String cpf = request.getParameter("txtCpf");
		String dataNascimento = request.getParameter("dateDataNascimento");

		if (request.getParameter("dateDataNascimento") == null) {

			dataNascimento = "";

		}

		System.out.println("- ID Cliente na Request" + request.getParameter("idCli"));

		String email = request.getParameter("txtEmail");
		String ativo = "ATIVO";
		int idCliente = Integer.valueOf(request.getParameter("idCli"));

		if (dataNascimento != null) {
			cliente.setDataNascimento(dataNascimento);
		}

		if (!nomeCompleto.trim().equals("") || nomeCompleto != null) {
			cliente.setNomeCompleto(nomeCompleto);
			System.out.println("- nome completo para alterar: " + cliente.getNomeCompleto());
		} else {
			cliente.setNomeCompleto("");
		}

		if (!cpf.trim().equals("") || cpf != null) {
			cliente.setCpf(cpf);
		} else {
			cliente.setCpf("");
		}

		if (!email.trim().equals("") || email != null) {
			cliente.setEmail(email);
		} else {
			cliente.setEmail("");
		}

		Senha senha = new Senha();
		senha.setSenha(null);

		cliente.setSenha(senha);

		cliente.setAtivo(ativo);

		cliente.setId(idCliente);

		return cliente;
	}

}
