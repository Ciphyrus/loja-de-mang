
package view.command;

import modelo.dominio.EntidadeDominio;
import modelo.dominio.Resultado;

public class ExcluirCommand extends AbstractCommand {

    @Override
    public Resultado executar(EntidadeDominio entidadeDominio) {
        return fachada.excluir(entidadeDominio);
    }
    
}
