package view.command;

import controle.fachada.Fachada;
import controle.fachada.IFachada;

public abstract class AbstractCommand implements ICommand{

	protected IFachada fachada = new Fachada();
}
