package view.command;

import modelo.dominio.EntidadeDominio;
import modelo.dominio.Resultado;

public class ConsultarCommand extends AbstractCommand {

	@Override
	public Resultado executar(EntidadeDominio ent) {
		return fachada.consultar(ent);
	}
}
