<%@page import="modelo.dominio.Pedido"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Alterar Pedido</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

    <!-- CSS here -->
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/flaticon.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/slicknav.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/animate.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/themify-icons.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/slick.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/nice-select.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/style.css">
		
	<!-- Script here -->
	
</head>
<body>
    <header>
        <!-- Header Start -->
        <div class="header-area">
            <div class="main-header header-sticky">
                <div class="container-fluid">
                    <div class="menu-wrapper">
                        <!-- Logo -->
                        <div class="logo">
                            <a href="<%=request.getContextPath()%>/index.jsp"><img class="manga-logo" src="assets/img/book-icon.png" alt=""></a>
                        </div>
                        <!-- Main-menu -->
                        <div class="main-menu d-none d-lg-block">
                            <nav>                                                
                                <ul id="navigation">  
                                    <li><a href="<%=request.getContextPath()%>/index.jsp">In�cio</a></li>
                                    <li><a href="<%=request.getContextPath()%>/shop.jsp">Produtos</a></li>
                                </ul>
                            </nav>
                        </div>
                        <!-- Header Right -->
                        <div class="header-right">
                            <ul>
                                <li> <a href="<%=request.getContextPath()%>/login.jsp"><span class="flaticon-user"></span></a></li>
                                <li><a href="<%=request.getContextPath()%>/autenticado/cart.jsp"><span class="flaticon-shopping-cart"></span></a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Mobile Menu -->
                    <div class="col-12">
                        <div class="mobile_menu d-block d-lg-none"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header End -->
    </header>
    <%
    	Pedido ped = (Pedido) session.getAttribute("VisualizarPed");
    %>
    <main>
        <!--================register_part Area =================-->
        <form action="AlterarManga" method="post">
       		<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-8 col-md-8">
						<h3 class="mb-30">Altera��o do pedido <%=ped.getId() %> do Cliente <%=ped.getCliente() %></h3>
						
						
						<div class="mt-10">
							<input type="text" name="txtCliente" id="txtCliente" placeholder="Cliente" value="<%=ped.getCliente() %>"
								onfocus="this.placeholder = ''" onblur="this.placeholder = 'Cliente'" required
								class="single-input">
						</div>
						
						
						
						
						<div class="mt-10">
							<div class="row">
								<div class="col-lg-12">
									<blockquote class="generic-blockquote">
											<center><h5>Endere�o</h5></center>
											
											<select id="txtTipoEnd" name="txtTipoEnd">
												<option selected="selected" value="Escolha o Estado">Escolha o Tipo de Endere�o</option>
												<% if (ped.getEndereco().getTipo() == "Residencial"){
													
												} else if (ped.getEndereco().getTipo() == "Comercial"){
													
												} else {
													
												}
												%>
												
												<option value="Residencial">Residencial</option>
												<option value="Comercial">Comercial</option>
												<option value="Cobranca">Cobranca</option>
											</select>
											
											<select id="selectEstado" name="selectEstado">
												<option selected="selected" value="Escolha o Estado">Escolha o Estado</option>
												<option value=""></option>
												<option value="SP">SP</option>
											</select>
											<input type="text" placeholder="Logradouro" name="txtLogradouro" id="Logradouro"
												onfocus="this.placeholder = ''" onblur="this.placeholder = 'Logradouro'" required
												class="single-input">
											<input type="text" placeholder="Bairro" name="txtBairro" id="txtBairro"
												onfocus="this.placeholder = ''" onblur="this.placeholder = 'Bairro'" required
												class="single-input">
											<input type="text" placeholder="Cidade" name="txtCidade" id="txtCidade"
												onfocus="this.placeholder = ''" onblur="this.placeholder = 'Cidade'" required
												class="single-input">
											<input type="text" placeholder="Numero" name="txtNumero" id="txtNumero"
												onfocus="this.placeholder = ''" onblur="this.placeholder = 'Numero'" required
												class="single-input">
											<input type="text" placeholder="CEP" name="txtCep" id="txtCep"
												onfocus="this.placeholder = ''" onblur="this.placeholder = 'CEP'" required
												class="single-input">
												
											<input type="text" placeholder="Complemento" name="txtComplemento" id="txtComplemento"
											onfocus="this.placeholder = ''" onblur="this.placeholder = 'Complemento'"
											class="single-input">
									</blockquote>
								</div>
							</div>
						</div>
						
						
						
						
						<div class="mt-10">
							<input type="text" name="txtCartao" id="txtCartao" placeholder="Cart�o de cr�dito" value="<%=ped.getCartao() %>"
								onfocus="this.placeholder = ''" onblur="this.placeholder = 'Cart�o de cr�dito'" required
								class="single-input">
						</div>
						
						
						
						<h3>Aqui fica o editar Mang�s</h3>
						
						
						
						<div class="mt-10">
							<input type="text" name="txtDataPedido" id="txtDataPedido" placeholder="Data do Pedido" value="<%=ped.getDataPedido() %>"
								onfocus="this.placeholder = ''" onblur="this.placeholder = 'Data do Pedido'" required
								class="single-input">
						</div>
						
						<div class="mt-10">
							<input type="text" name="txtDataPagamento" id="txtDataPagamento" placeholder="Data de Pagamento" value="<%=ped.getDataPagamento() %>"
								onfocus="this.placeholder = ''" onblur="this.placeholder = 'Data de Pagamento'" required
								class="single-input">
						</div>
						
						<div class="mt-10">
							<input type="text" name="txtValor" id="txtValor" placeholder="Valor total" value="<%=ped.getValorTotal() %>"
								onfocus="this.placeholder = ''" onblur="this.placeholder = 'Valor total'" required
								class="single-input">
						</div>
						
						<div class="mt-10">
							<input type="text" name="txtStatus" id="txtStatus" placeholder="Status" value="<%=ped.getStatus() %>"
								onfocus="this.placeholder = ''" onblur="this.placeholder = 'Status'" required
								class="single-input">
						</div>
						
						<input type="hidden" id="idPed" name="idPed" value="<%=ped.getId()%>">
						
						<div class="mt-10 my-5">
							<button type="submit" id="OPERACAO" name="OPERACAO" value="ALTERAR" class="btn_3" href="pedido-list.jsp">
								Salvar
							</button>
						</div>
						
					</div>
				</div>
			</div>
       	</form>
        <!--================register_part end =================-->
    </main>
    <footer>

    </footer>
    <!--? Search model Begin -->
    <!-- Search model end -->
    
    <!-- JS here -->
    
    <script src="./assets/js/funcoes.js"></script>

    <script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
    <!-- Jquery, Popper, Bootstrap -->
    <script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="./assets/js/popper.min.js"></script>
    <script src="./assets/js/bootstrap.min.js"></script>
    <!-- Jquery Mobile Menu -->
    <script src="./assets/js/jquery.slicknav.min.js"></script>

    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src="./assets/js/owl.carousel.min.js"></script>

    <!-- One Page, Animated-HeadLin -->
    <script src="./assets/js/wow.min.js"></script>
    <script src="./assets/js/animated.headline.js"></script>
    
    <!-- Scroll up, nice-select, sticky -->
    <script src="./assets/js/jquery.scrollUp.min.js"></script>
    <script src="./assets/js/jquery.nice-select.min.js"></script>
    <script src="./assets/js/jquery.sticky.js"></script>
    <script src="./assets/js/jquery.magnific-popup.js"></script>

    <!-- contact js -->
    <script src="./assets/js/contact.js"></script>
    <script src="./assets/js/jquery.form.js"></script>
    <script src="./assets/js/jquery.validate.min.js"></script>
    <script src="./assets/js/mail-script.js"></script>
    <script src="./assets/js/jquery.ajaxchimp.min.js"></script>
    
    <!-- Jquery Plugins, main Jquery -->	
    <script src="./assets/js/plugins.js"></script>
    <script src="./assets/js/main.js"></script>

	<!-- JQuery Mask -->
	<script type="text/javascript" src="assets/js/mask/dist/jquery.mask.min.js"></script>
	<script type="text/javascript" src="assets/js/mask/dist/aplica.js"></script>
</body>
    
</html>