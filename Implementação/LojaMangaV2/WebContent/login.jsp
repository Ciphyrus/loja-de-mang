<!doctype html>
<%@page import="modelo.dominio.Resultado"%>
<html lang="zxx">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Login</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="site.webmanifest">
<link rel="shortcut icon" type="image/x-icon"
	href="assets/img/favicon.ico">

<!-- CSS here -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/flaticon.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/slicknav.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/animate.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/themify-icons.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/slick.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/nice-select.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/style.css">
</head>
<body>
	<header>
		<!-- Header Start -->
		<div class="header-area">
			<div class="main-header header-sticky">
				<div class="container-fluid">
					<div class="menu-wrapper">
						<!-- Logo -->
						<div class="logo">
							<a href="<%=request.getContextPath()%>/index.jsp"><img class="manga-logo"
								src="assets/img/book-icon.png" alt=""></a>
						</div>
						<!-- Main-menu -->
						<div class="main-menu d-none d-lg-block">
							<nav>
								<ul id="navigation">
									<li><a href="<%=request.getContextPath()%>/index.jsp">In�cio</a></li>
									<li><a href="<%=request.getContextPath()%>/shop.jsp">Produtos</a></li>
								</ul>
							</nav>
						</div>
						<!-- Header Right -->
						<div class="header-right">
							<ul>
								<li><a href="<%=request.getContextPath()%>/login.jsp"><span class="flaticon-user"></span></a></li>
								<li><a href="<%=request.getContextPath()%>/autenticado/cart.jsp"><span
										class="flaticon-shopping-cart"></span></a></li>
							</ul>
						</div>
					</div>
					<!-- Mobile Menu -->
					<div class="col-12">
						<div class="mobile_menu d-block d-lg-none"></div>
					</div>
				</div>
			</div>
		</div>
		<!-- Header End -->
	</header>
	<main>
		<!--================login_part Area =================-->
		<section class="login_part pt-5">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-6 col-md-6">
						<div class="login_part_text text-center">
							<div class="login_part_text_iner">
								<h2>Novo na Loja de Mang� ?</h2>
								<p>Aqui voc� pode encontrar seu mang� por um valor acess�vel</p>
								<a href="customer-insert.jsp" class="btn_3">Criar uma conta</a>
							</div>
						</div>
					</div>

					<%
						Resultado resultado = (Resultado) session.getAttribute("resultado");
					%>

					<div class="col-lg-6 col-md-6">
						<div class="login_part_form">
							<div class="login_part_form_iner">
								<h3>
									Bem vindo de volta ! <br> Por favor, fa�a seu login
								</h3>
								<form class="row contact_form" action="Login" method="post"
									novalidate="novalidate">
									<div class="col-md-12 form-group p_star">
										<input type="email" class="form-control" id="txtEmail"
											name="txtEmail" required placeholder="Email">
									</div>
									<div class="col-md-12 form-group p_star">
										<input type="password" class="form-control" id="txtSenha"
											name="txtSenha" required placeholder="Senha">
									</div>
									<div class="col-md-12 form-group">
										<div class="creat_account d-flex align-items-center">
											<input type="checkbox" id="f-option" name="selector">
											<label for="f-option">Lembrar de mim</label>
										</div>
										<%
											if (resultado != null) {
											if (resultado.getMsg() != null) {
										%>
										<div class="alert alert-danger" role="alert">
											<%=resultado.getMsg()%>
										</div>
										<%
											}
										}
										%>

										<button type="submit" id="OPERACAO" name="OPERACAO"
											value="LOGIN" class="btn_3">log in</button>
										<a class="lost_pass" href="#">Esqueceu senha?</a>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--================login_part end =================-->
	</main>
	<footer> </footer>
	<!--? Search model Begin -->
	<!-- Search model end -->

	<!-- JS here -->

	<script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
	<!-- Jquery, Popper, Bootstrap -->
	<script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
	<script src="./assets/js/popper.min.js"></script>
	<script src="./assets/js/bootstrap.min.js"></script>
	<!-- Jquery Mobile Menu -->
	<script src="./assets/js/jquery.slicknav.min.js"></script>

	<!-- Jquery Slick , Owl-Carousel Plugins -->
	<script src="./assets/js/owl.carousel.min.js"></script>
	<script src="./assets/js/slick.min.js"></script>

	<!-- One Page, Animated-HeadLin -->
	<script src="./assets/js/wow.min.js"></script>
	<script src="./assets/js/animated.headline.js"></script>

	<!-- Scroll up, nice-select, sticky -->
	<script src="./assets/js/jquery.scrollUp.min.js"></script>
	<script src="./assets/js/jquery.nice-select.min.js"></script>
	<script src="./assets/js/jquery.sticky.js"></script>
	<script src="./assets/js/jquery.magnific-popup.js"></script>

	<!-- contact js -->
	<script src="./assets/js/contact.js"></script>
	<script src="./assets/js/jquery.form.js"></script>
	<script src="./assets/js/jquery.validate.min.js"></script>
	<script src="./assets/js/mail-script.js"></script>
	<script src="./assets/js/jquery.ajaxchimp.min.js"></script>

	<!-- Jquery Plugins, main Jquery -->
	<script src="./assets/js/plugins.js"></script>
	<script src="./assets/js/main.js"></script>

</body>

</html>