<!-- @page import="jdk.internal.misc.FileSystemOption" -->
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List" %>
<%@page import="modelo.dominio.Resultado"%>
<%@page import="modelo.dominio.EntidadeDominio"%>
<%@page import="modelo.dominio.Endereco"%>
<%@page import="modelo.dominio.CartaoCredito"%>
<%@page import="modelo.dominio.Manga"%>
<%@page import="modelo.dominio.Carrinho"%>
<%@page import="modelo.dominio.Cliente"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="zxx">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Loja de Mang� - Checkout de Pedido</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="site.webmanifest">
<link rel="shortcut icon" type="image/x-icon"
	href="assets/img/favicon.ico">

<!-- CSS here -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/flaticon.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/slicknav.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/animate.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/themify-icons.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/slick.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/nice-select.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/style.css">
</head>

<body>

	<header>
		<!-- Header Start -->
		<div class="header-area">
			<div class="main-header header-sticky">
				<div class="container-fluid">
					<div class="menu-wrapper">
						<!-- Logo -->
						<div class="logo">
							<a href="<%=request.getContextPath()%>/index.jsp"><img class="manga-logo"
								src="assets/img/book-icon.png" alt=""></a>
						</div>
						<!-- Main-menu -->
						<div class="main-menu d-none d-lg-block">
							<nav>
								<ul id="navigation">
									<li><a href="<%=request.getContextPath()%>/index.jsp">Inicio</a></li>
									<li><a href="<%=request.getContextPath()%>/shop.jsp">Produtos</a></li>
								</ul>
							</nav>
						</div>
						<!-- Header Right -->
						<div class="header-right">
							<ul>
								<li><a href="<%=request.getContextPath()%>/login.jsp"><span class="flaticon-user"></span></a></li>
								<li><a href="<%=request.getContextPath()%>/autenticado/cart.jsp"><span
										class="flaticon-shopping-cart"></span></a></li>
							</ul>
						</div>
					</div>
					<!-- Mobile Menu -->
					<div class="col-12">
						<div class="mobile_menu d-block d-lg-none"></div>
					</div>
				</div>
			</div>
		</div>
		<!-- Header End -->
	</header>

	<% 
  	
  	Cliente cli = (Cliente) session.getAttribute("clienteLogado");
  	if (cli == null){
  		cli = new Cliente();
  	}
  
  	Carrinho car;
	if (session.getAttribute("carrinho") != null) {
		car = (Carrinho) session.getAttribute("carrinho");
	} else {
		car = new Carrinho();
	}
  %>
	<main>
		<section class="checkout_area pt-5">
			<form class="row contact_form" action="../SalvarPedido" method="post">
				<div class="container">
					<div class="billing_details">
						<div class="row">
							<div class="col-lg-6">
								<h3>Carrinho</h3>
								<div class="col-md-12 form-group p_star">
									<select class="country_select" id="cartaoPed" name="cartaoPed">
									
										<%
											for (EntidadeDominio j: cli.getCartaoCredito()) {
												CartaoCredito cc = (CartaoCredito) j;
										%>
											<option value="<%=cc.getId() %>">Cart�o <%= cc.getNumeroCartao() %> Id:<%=cc.getId() %></option>
										<% } %>

									</select>
								</div>
								<div class="col-md-12 form-group p_star">
									<select class="country_select" id="endPed" name="endPed">
										
										<% for (EntidadeDominio i: cli.getEndereco()) { 
												Endereco end = (Endereco) i;
										%>
											
											<option value="<%=end.getId()%>"><%=end.getLogradouro() %> Id: <%=end.getId() %></option>
										<% } %>
										
										
									</select>
								</div>
							</div>

							<div class="col-lg-6">
								<div class="order_box">
									<h2>Seu Pedido</h2>
									<ul class="list">
										<li><a href="#">Produto <span>Total</span>
										</a></li>
										<%
					                      for(Manga mg: car.getMangas()){
					                    	  
					                      %>
										<li><a href="#"><%= mg.getTitulo() %> - Volume <%= mg.getEdicao() %>
												- Quantidade <%= mg.getQuantidadeProdutoCarrinho() %> <span
												class="last">R$ <%=mg.getPreco() * mg.getQuantidadeProdutoCarrinho() %>
													(TOTAL)
											</span> </a></li>
										<%
					                      	}
					                      %>
									</ul>
									<a value="SALVAR" id="OPERACAO" href="confirmation.jsp"
										name="OPERACAO">Confirmar Pagamento</a>
								</div>
							</div>

						</div>
					</div>
				</div>
			</form>
		</section>
		<!--================End Checkout Area =================-->
	</main>
	<!--? Search model Begin -->
	<div class="search-model-box">
		<div class="h-100 d-flex align-items-center justify-content-center">
			<div class="search-close-btn">+</div>
			<form class="search-model-form">
				<input type="text" id="search-input"
					placeholder="Searching key.....">
			</form>
		</div>
	</div>
	<!-- Search model end -->

	<!-- JS here -->

	<script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
	<!-- Jquery, Popper, Bootstrap -->
	<script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
	<script src="./assets/js/popper.min.js"></script>
	<script src="./assets/js/bootstrap.min.js"></script>
	<!-- Jquery Mobile Menu -->
	<script src="./assets/js/jquery.slicknav.min.js"></script>

	<!-- Jquery Slick , Owl-Carousel Plugins -->
	<script src="./assets/js/owl.carousel.min.js"></script>
	<script src="./assets/js/slick.min.js"></script>

	<!-- One Page, Animated-HeadLin -->
	<script src="./assets/js/wow.min.js"></script>
	<script src="./assets/js/animated.headline.js"></script>
	<script src="./assets/js/jquery.magnific-popup.js"></script>

	<!-- Scroll up, nice-select, sticky -->
	<script src="./assets/js/jquery.scrollUp.min.js"></script>
	<script src="./assets/js/jquery.nice-select.min.js"></script>
	<script src="./assets/js/jquery.sticky.js"></script>

	<!-- contact js -->
	<script src="./assets/js/contact.js"></script>
	<script src="./assets/js/jquery.form.js"></script>
	<script src="./assets/js/jquery.validate.min.js"></script>
	<script src="./assets/js/mail-script.js"></script>
	<script src="./assets/js/jquery.ajaxchimp.min.js"></script>

	<!-- Jquery Plugins, main Jquery -->
	<script src="./assets/js/plugins.js"></script>
	<script src="./assets/js/main.js"></script>

</body>
</html>