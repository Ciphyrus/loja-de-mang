<!doctype html>
<%@page import="modelo.dominio.Carrinho"%>
<%@page import="modelo.dominio.Manga"%>
<html lang="zxx">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Loja de Mangá - Carrinho</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="site.webmanifest">
<link rel="shortcut icon" type="image/x-icon"
	href="assets/img/favicon.ico">

<!-- CSS here -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/flaticon.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/slicknav.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/animate.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/themify-icons.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/slick.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/nice-select.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/style.css">
</head>

<body>
	<header>
		<!-- Header Start -->
		<div class="header-area">
			<div class="main-header header-sticky">
				<div class="container-fluid">
					<div class="menu-wrapper">
						<!-- Logo -->
						<div class="logo">
							<a href="<%=request.getContextPath()%>/index.jsp"><img class="manga-logo"
								src="assets/img/book-icon.png" alt=""></a>
						</div>
						<!-- Main-menu -->
						<div class="main-menu d-none d-lg-block">
							<nav>
								<ul id="navigation">
									<li><a href="<%=request.getContextPath()%>/index.jsp">In�cio</a></li>
									<li><a href="<%=request.getContextPath()%>/shop.jsp">Produtos</a></li>
								</ul>
							</nav>
						</div>
						<!-- Header Right -->
						<div class="header-right">
							<ul>
								<li><a href="<%=request.getContextPath()%>/login.jsp"><span class="flaticon-user"></span></a></li>
								<li><a href="<%=request.getContextPath()%>/autenticado/cart.jsp"><span
										class="flaticon-shopping-cart"></span></a></li>
							</ul>
						</div>
					</div>
					<!-- Mobile Menu -->
					<div class="col-12">
						<div class="mobile_menu d-block d-lg-none"></div>
					</div>
				</div>
			</div>
		</div>
		<!-- Header End -->
	</header>
	<main>

		<%
			Carrinho car;
			if (session.getAttribute("carrinho") != null) {
				car = (Carrinho) session.getAttribute("carrinho");
			} else {
				car = new Carrinho();
			}
		%>
		
		
		<!--================Cart Area =================-->
		<section class="cart_area pt-5">
			<div class="container">
				<div class="cart_inner">
					<div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th scope="col">Produto</th>
									<th scope="col">Pre�o</th>
									<th scope="col">Quantidade</th>
									<th scope="col">Total</th>
								</tr>
							</thead>
							
							
							<tbody>
								<%
									int valor_total = 0;
									for (Manga mg: car.getMangas()){
								%>
								<tr>
									<td>
										<div class="media">
											<div class="d-flex">
												<img src="assets/img/<%= mg.getEndereco_imagem()%>"
													alt="" />
											</div>
											<div class="media-body">
												<p><%= mg.getTitulo() %> - Volume <%= mg.getEdicao() %></p>
											</div>
										</div>
									</td>
									<td>
										<h5>R$<%= mg.getPreco() %></h5>
									</td>
									<td>
										<div class="product_count">
											<span class="input-number-decrement"> <i
												class="ti-minus"></i></span> <input class="input-number"
												type="text" value=<%= mg.getQuantidadeProdutoCarrinho() %> min="0" max="10"> <span
												class="input-number-increment"> <i class="ti-plus"></i></span>
										</div>
									</td>
									<td>
										<h5>R$<%=mg.getPreco() * mg.getQuantidadeProdutoCarrinho() %></h5>
									</td>
								</tr>
								
								<%
										valor_total += mg.getPreco() * mg.getQuantidadeProdutoCarrinho();
									}
								%>
								
								<tr>
									<td></td>
									<td></td>
									<td>
										<h5>Subtotal</h5>
									</td>
									<td>
										<h5>R$<%=valor_total %></h5>
									</td>
								</tr>
								<tr class="shipping_area">
									<td></td>
									<td></td>
									<td>
										<h5>Frete</h5>
									</td>
									<td>
										<div class="shipping_box">
											<ul class="list">
												<li>Frete Gr�tis <input type="radio"
													aria-label="Radio button for following text input">
												</li>
											</ul>
										</div>
									</td>
								</tr>
							</tbody>
							
							
						</table>
						<div class="checkout_btn_inner float-right">
							<a class="btn_1" href="<%=request.getContextPath()%>/shop.jsp">Continuar Comprando</a> <a
								class="btn_1 checkout_btn_1" href="<%=request.getContextPath()%>/autenticado/checkout.jsp">Seguir
								para checkout</a>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--================End Cart Area =================-->
	</main>
	>

	<!-- JS here -->

	<script src="assets/js/vendor/modernizr-3.5.0.min.js"></script>
	<!-- Jquery, Popper, Bootstrap -->
	<script src="assets/js/vendor/jquery-1.12.4.min.js"></script>
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<!-- Jquery Mobile Menu -->
	<script src="assets/js/jquery.slicknav.min.js"></script>

	<!-- Jquery Slick , Owl-Carousel Plugins -->
	<script src="assets/js/owl.carousel.min.js"></script>
	<script src="assets/js/slick.min.js"></script>

	<!-- One Page, Animated-HeadLin -->
	<script src="assets/js/wow.min.js"></script>
	<script src="assets/js/animated.headline.js"></script>

	<!-- Scrollup, nice-select, sticky -->
	<script src="assets/js/jquery.scrollUp.min.js"></script>
	<script src="assets/js/jquery.nice-select.min.js"></script>
	<script src="assets/js/jquery.sticky.js"></script>
	<script src="assets/js/jquery.magnific-popup.js"></script>

	<!-- contact js -->
	<script src="assets/js/contact.js"></script>
	<script src="assets/js/jquery.form.js"></script>
	<script src="assets/js/jquery.validate.min.js"></script>
	<script src="assets/js/mail-script.js"></script>
	<script src="assets/js/jquery.ajaxchimp.min.js"></script>

	<!-- Jquery Plugins, main Jquery -->
	<script src="assets/js/plugins.js"></script>
	<script src="assets/js/main.js"></script>

</body>
</html>