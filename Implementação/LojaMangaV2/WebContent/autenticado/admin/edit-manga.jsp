<%@page import="modelo.dominio.Manga"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="zxx">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Alterar Produto</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="site.webmanifest">
<link rel="shortcut icon" type="image/x-icon"
	href="assets/img/favicon.ico">

<!-- CSS here -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/flaticon.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/slicknav.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/animate.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/themify-icons.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/slick.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/nice-select.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/style.css">

<!-- Script here -->

</head>
<body>
	<header>
		<!-- Header Start -->
		<div class="header-area">
			<div class="main-header header-sticky">
				<div class="container-fluid">
					<div class="menu-wrapper">
						<!-- Logo -->
						<div class="logo">
							<a href="<%=request.getContextPath()%>/index.jsp"><img class="manga-logo"
								src="assets/img/book-icon.png" alt=""></a>
						</div>
						<!-- Main-menu -->
						<div class="main-menu d-none d-lg-block">
							<nav>
								<ul id="navigation">
									<li><a href="<%=request.getContextPath()%>/index.jsp">In�cio</a></li>
									<li><a href="<%=request.getContextPath()%>/shop.jsp">Produtos</a></li>
								</ul>
							</nav>
						</div>
						<!-- Header Right -->
						<div class="header-right">
							<ul>
								<li><a href="<%=request.getContextPath()%>/login.jsp"><span
										class="flaticon-user"></span></a></li>
								<li><a href="<%=request.getContextPath()%>/autenticado/cart.jsp"><span
										class="flaticon-shopping-cart"></span></a></li>
							</ul>
						</div>
					</div>
					<!-- Mobile Menu -->
					<div class="col-12">
						<div class="mobile_menu d-block d-lg-none"></div>
					</div>
				</div>
			</div>
		</div>
		<!-- Header End -->
	</header>
	<%
		Manga mg = (Manga) session.getAttribute("VisualizarMg");
	%>
	<main>
		<!--================register_part Area =================-->
		<form action="../../AlterarManga" method="post">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-8 col-md-8">
						<h3 class="mb-30">
							Altera��o do produto
							<%=mg.getTitulo()%>
							- Volume
							<%=mg.getEdicao()%></h3>

						<div class="mt-10">
							<input type="text" name="txtTitulo" id="txtTitulo"
								placeholder="T�tulo" value="<%=mg.getTitulo()%>"
								onfocus="this.placeholder = ''"
								onblur="this.placeholder = 'T�tulo'" required
								class="single-input">
						</div>

						<div class="mt-10">
							<input type="text" name="txtAutor" id="txtAutor"
								placeholder="Autor" value="<%=mg.getAutor()%>"
								onfocus="this.placeholder = ''"
								onblur="this.placeholder = 'Autor'" required
								class="single-input">
						</div>

						<div class="mt-10">
							<input type="text" name="txtAno" id="txtAno" placeholder="Ano"
								value=<%=mg.getAno()%> onfocus="this.placeholder = ''"
								onblur="this.placeholder = 'Ano'" required class="single-input">
						</div>

						<div class="mt-10">
							<input type="text" name="txtEditora" id="txtEditora"
								placeholder="Editora" value=<%=mg.getEditora()%>
								onfocus="this.placeholder = ''"
								onblur="this.placeholder = 'Editora'" required
								class="single-input">
						</div>

						<div class="mt-10">
							<input type="text" name="txtEdicao" id="txtEdicao"
								placeholder="Edi��o" value=<%=mg.getEdicao()%>
								onfocus="this.placeholder = ''"
								onblur="this.placeholder = 'Edi��o'" required
								class="single-input">
						</div>

						<div class="mt-10">
							<input type="text" name="txtIsbn" id="txtIsbn" placeholder="ISBN"
								value=<%=mg.getIsbn()%> onfocus="this.placeholder = ''"
								onblur="this.placeholder = 'ISBN'" required class="single-input">
						</div>

						<div class="mt-10">
							<input type="text" name="txtCodigoBarras" id="txtCodigoBarras"
								placeholder="C�digo de barras" value=<%=mg.getCodigoBarras()%>
								onfocus="this.placeholder = ''"
								onblur="this.placeholder = 'C�digo de barras'" required
								class="single-input">
						</div>

						<div class="mt-10">
							<input type="text" name="txtPaginas" id="txtPaginas"
								placeholder="N�mero de P�ginas" value=<%=mg.getPaginas()%>
								onfocus="this.placeholder = ''"
								onblur="this.placeholder = 'N�mero de P�ginas'" required
								class="single-input">
						</div>

						<div class="mt-10">
							<textarea class="single-textarea" placeholder="Sin�pse"
								name="txtSinopse" id="txtSinopse"
								onfocus="this.placeholder = ''"
								onblur="this.placeholder = 'Sin�pse'" required><%=mg.getSinopse()%></textarea>
						</div>

						<div class="mt-10">
							<input type="text" name="txtPrecificacao" id="txtPrecificacao"
								placeholder="Grupo de Pecifica��o"
								value=<%=mg.getGrupoPrecificacao()%>
								onfocus="this.placeholder = ''"
								onblur="this.placeholder = 'Grupo de Precifica��o'" required
								class="single-input">
						</div>

						<div class="mt-10">
							<input type="text" name="txtQuantidade" id="txtQuantidade"
								placeholder="Quantidade" value=<%=mg.getQuantidade()%>
								onfocus="this.placeholder = ''"
								onblur="this.placeholder = 'Quantidade'" required
								class="single-input">
						</div>

						<div class="mt-10">
							<input type="text" name="txtPreco" id="txtPreco"
								placeholder="Pre�o" value=<%=mg.getPreco()%>
								onfocus="this.placeholder = ''"
								onblur="this.placeholder = 'Quantidade'" required
								class="single-input">
						</div>

						<div class="mt-10">
							<div class="row">
								<div class="col-lg-12">
									<blockquote class="generic-blockquote">
										<center>
											<h5>Dimens�es</h5>
										</center>
										<input type="text" name="txtAltura" id="txtAltura"
											placeholder="Altura" value=<%=mg.getAltura()%>
											onfocus="this.placeholder = ''"
											onblur="this.placeholder = 'Altura'" required
											class="single-input">
								 		<input type="text" name="txtLargura" id="txtLargura" placeholder="Largura"
											value=<%=mg.getLargura()%> onfocus="this.placeholder = ''"
											onblur="this.placeholder = 'Largura'" required
											class="single-input"> 
										<input type="text" name="txtProfundidade" id="txtProfundidade"
											placeholder="Profundidade" value=<%=mg.getProfundidade()%>
											onfocus="this.placeholder = ''"
											onblur="this.placeholder = 'Profundidade'" required
											class="single-input">
									 	<input type="text" name="txtPeso" id="txtPeso" placeholder="Peso"
											value=<%=mg.getPeso()%> onfocus="this.placeholder = ''"
											onblur="this.placeholder = 'Peso'" required
											class="single-input">
									</blockquote>
								</div>
							</div>
						</div>

						<div class="mt-10">
							<input type="file" name="mgImagem" id="mgImagem" accept="image/*"
								placeholder="Imagem" value=<%=mg.getEndereco_imagem()%>
								onfocus="this.placeholder = ''"
								onblur="this.placeholder = 'Imagem'" required
								class="single-input">
						</div>

						<input type="hidden" id="idMg" name="idMg" value="<%=mg.getId()%>">

						<div class="mt-10 my-5">
							<button type="submit" id="OPERACAO" name="OPERACAO"
								value="ALTERAR" class="btn_3">
								Salvar</button>
						</div>

					</div>
				</div>
			</div>
		</form>
		<!--================register_part end =================-->
	</main>
	<footer> </footer>
	<!--? Search model Begin -->
	<!-- Search model end -->

	<!-- JS here -->

	<script src="./assets/js/funcoes.js"></script>

	<script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
	<!-- Jquery, Popper, Bootstrap -->
	<script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
	<script src="./assets/js/popper.min.js"></script>
	<script src="./assets/js/bootstrap.min.js"></script>
	<!-- Jquery Mobile Menu -->
	<script src="./assets/js/jquery.slicknav.min.js"></script>

	<!-- Jquery Slick , Owl-Carousel Plugins -->
	<script src="./assets/js/owl.carousel.min.js"></script>

	<!-- One Page, Animated-HeadLin -->
	<script src="./assets/js/wow.min.js"></script>
	<script src="./assets/js/animated.headline.js"></script>

	<!-- Scroll up, nice-select, sticky -->
	<script src="./assets/js/jquery.scrollUp.min.js"></script>
	<script src="./assets/js/jquery.nice-select.min.js"></script>
	<script src="./assets/js/jquery.sticky.js"></script>
	<script src="./assets/js/jquery.magnific-popup.js"></script>

	<!-- contact js -->
	<script src="./assets/js/contact.js"></script>
	<script src="./assets/js/jquery.form.js"></script>
	<script src="./assets/js/jquery.validate.min.js"></script>
	<script src="./assets/js/mail-script.js"></script>
	<script src="./assets/js/jquery.ajaxchimp.min.js"></script>

	<!-- Jquery Plugins, main Jquery -->
	<script src="./assets/js/plugins.js"></script>
	<script src="./assets/js/main.js"></script>

	<!-- JQuery Mask -->
	<script type="text/javascript"
		src="assets/js/mask/dist/jquery.mask.min.js"></script>
	<script type="text/javascript" src="assets/js/mask/dist/aplica.js"></script>
</body>

</html>