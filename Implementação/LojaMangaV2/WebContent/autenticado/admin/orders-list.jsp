<%@page import="modelo.dominio.Cliente"%>
<%@page import="modelo.dao.PedidoDAO"%>
<%@page import="modelo.dominio.Pedido"%>
<%@page import="modelo.dominio.EntidadeDominio"%>
<%@page import="java.util.List"%>
<%@page import="modelo.dominio.Resultado"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Lista de Pedidos</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

    <!-- CSS here -->
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/flaticon.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/slicknav.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/animate.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/themify-icons.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/slick.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/nice-select.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/style.css">
</head>
<body>
    <header>
        <!-- Header Start -->
        <div class="header-area">
            <div class="main-header header-sticky">
                <div class="container-fluid">
                    <div class="menu-wrapper">
                        <!-- Logo -->
                        <div class="logo">
                            <a href="<%=request.getContextPath()%>/index.jsp"><img class="manga-logo" src="assets/img/book-icon.png" alt=""></a>
                        </div>
                        <!-- Main-menu -->
                        <div class="main-menu d-none d-lg-block">
                            <nav>                                                
                                <ul id="navigation">  
                                    <li><a href="<%=request.getContextPath()%>/index.jsp">In�cio</a></li>
                                    <li><a href="<%=request.getContextPath()%>/shop.jsp">Produtos</a></li>
                                    <li><a href="#">P�ginas</a>
                                        <ul class="submenu">
                                            <li><a href="<%=request.getContextPath()%>/login.jsp">Login</a></li>
                                            <li><a href="<%=request.getContextPath()%>/autenticado/cart.jsp">Carrinho</a></li>
                                            <li><a href="<%=request.getContextPath()%>/autenticado/confirmation.jsp">Confirma��o</a></li>
                                            <li><a href="<%=request.getContextPath()%>/autenticado/checkout.jsp">Checkout</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <!-- Header Right -->
                        <div class="header-right">
                            <ul>
                                <li> <a href="<%=request.getContextPath()%>/login.jsp"><span class="flaticon-user"></span></a></li>
                                <li><a href="<%=request.getContextPath()%>/autenticado/cart.jsp"><span class="flaticon-shopping-cart"></span></a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Mobile Menu -->
                    <div class="col-12">
                        <div class="mobile_menu d-block d-lg-none"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header End -->
    </header>   
    <main>
        <div class="container">
            <section class="section-top-border">
                <h3 class="mb-30">Lista de Pedidos</h3>
                <div class="table-responsive">
                    <div class="progress-table">
                        <table class="table">
                            <thead>
                                <th scope="col">ID</th>
                                <th scope="col">Cliente</th>
                                <th scope="col">Produtos</th>
                                <th scope="col">Data do pedido</th>
                                <th scope="col">Status</th>
                                <th scope="col">Visualizar</th>
                            </thead>
                            <% 

                        	Cliente cli = (Cliente) session.getAttribute("clienteLogado");
                        	Pedido ped = new Pedido();
                        	ped.setCliente(cli);
                        	ped.setStatus("todospedidos");
                        	PedidoDAO pedDao = new PedidoDAO();
                        	Resultado resultado = pedDao.consultar(ped);
                        	
                            %>
                            
                            <tbody>
	                            <%
	                            if(resultado.getEntidades() != null){
                           			for(EntidadeDominio i : resultado.getEntidades()){
                           			Pedido pedido = (Pedido) i;
	                           			
	                            %>
	                            <tr>
	                               	<td scope="row"><%= ped.getId()%></td>
	                               	<td scope="row"><%= ped.getCliente().getNomeCompleto()%></td>
	                               	<td scope="row"><%= ped.getMangas()%></td>
	                               	<td scope="row"><%= ped.getDataPedido()%></td>
	                               	<td scope="row"><%= ped.getStatus()%></td>
	                               	<td scope="row">
                            		<a href="<%=request.getContextPath()%>/pedido-view.jsp" class="btn_3" id="OPERACAO" name="OPERACAO">Visualizar</a>
	                               </td>
                               </tr>
                                <%
	                           			}
	                            	}
                                %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </main>
    <footer>

    </footer>
    <!--? Search model Begin -->
    <!-- Search model end -->
    
    <!-- JS here -->

    <script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
    <!-- Jquery, Popper, Bootstrap -->
    <script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="./assets/js/popper.min.js"></script>
    <script src="./assets/js/bootstrap.min.js"></script>
    <!-- Jquery Mobile Menu -->
    <script src="./assets/js/jquery.slicknav.min.js"></script>

    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src="./assets/js/owl.carousel.min.js"></script>
    <script src="./assets/js/slick.min.js"></script>

    <!-- One Page, Animated-HeadLin -->
    <script src="./assets/js/wow.min.js"></script>
    <script src="./assets/js/animated.headline.js"></script>
    
    <!-- Scroll up, nice-select, sticky -->
    <script src="./assets/js/jquery.scrollUp.min.js"></script>
    <script src="./assets/js/jquery.nice-select.min.js"></script>
    <script src="./assets/js/jquery.sticky.js"></script>
    <script src="./assets/js/jquery.magnific-popup.js"></script>

    <!-- contact js -->
    <script src="./assets/js/contact.js"></script>
    <script src="./assets/js/jquery.form.js"></script>
    <script src="./assets/js/jquery.validate.min.js"></script>
    <script src="./assets/js/mail-script.js"></script>
    <script src="./assets/js/jquery.ajaxchimp.min.js"></script>
    
    <!-- Jquery Plugins, main Jquery -->	
    <script src="./assets/js/plugins.js"></script>
    <script src="./assets/js/main.js"></script>
</body>
    
</html>