<%@page import="modelo.dominio.Cliente"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Loja de Manga - Cadastrar Manga</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

    <!-- CSS here -->
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/flaticon.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/slicknav.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/animate.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/themify-icons.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/slick.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/nice-select.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/style.css">
</head>
<body>
    <header>
        <!-- Header Start -->
        <div class="header-area">
            <div class="main-header header-sticky">
                <div class="container-fluid">
                    <div class="menu-wrapper">
                        <!-- Logo -->
                        <div class="logo">
                            <a href="<%=request.getContextPath()%>/index.jsp"><img class="manga-logo" src="assets/img/book-icon.png" alt=""></a>
                        </div>
                        <!-- Main-menu -->
                        <div class="main-menu d-none d-lg-block">
                            <nav>                                                
                                <ul id="navigation">  
                                    <li><a href="<%=request.getContextPath()%>/index.jsp">Inicio</a></li>
                                    <li><a href="<%=request.getContextPath()%>/shop.jsp">Produtos</a></li>
                                </ul>
                            </nav>
                        </div>
                        <!-- Header Right -->
                        <div class="header-right">
                            <ul>
                                <li> <a href="<%=request.getContextPath()%>/login.jsp"><span class="flaticon-user"></span></a></li>
                                <li><a href="<%=request.getContextPath()%>/autenticado/cart.jsp"><span class="flaticon-shopping-cart"></span></a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Mobile Menu -->
                    <div class="col-12">
                        <div class="mobile_menu d-block d-lg-none"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header End -->
    </header>
    
    <% 
    	Cliente cli = (Cliente) session.getAttribute("VisualizarCli");
    %>
    
    <main>
        <!--================register_part Area =================-->
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-8 col-md-8">
					<h3 class="mb-30">Editar Cadastro de Cliente</h3>
					
						<div class="container">
							<div class="row align-items-center">
								<div class="col-lg-8 col-md-8">
									<h3 class="mb-30">Cadastro</h3>
									
									<div class="mt-10">
										<input type="text" placeholder="Nome Completo" id="txtNomeCompleto" name="txtNomeCompleto"
											onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nome'" required value="<%=cli.getNomeCompleto()%>"
											class="single-input">
									</div>
									
									<div class="mt-10">
										<input type="email" placeholder="Email" name="txtEmail" id="txtEmail"
											onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email'" required value="<%=cli.getEmail()%>"
											class="single-input">
									</div>
									<div class="mt-10">
										<input type="password"placeholder="Senha" name="txtSenha" id="txtSenha"
											onfocus="this.placeholder = ''" onblur="this.placeholder = 'Senha'" required value="<%=cli.getSenha().getSenha()%>"
											class="single-input">
									</div>
									<div class="mt-10">
										<input type="password"placeholder="Confirmar Senha" name="txtConfirmaSenha" id="txtConfirmaSenha"
											onfocus="this.placeholder = ''" onblur="this.placeholder = 'Senha'" required 
											class="single-input">
									</div>
									
									<input type="date" placeholder="Data de Nascimento" name="dateDataNascimento" id="dateDataNascimento"
															onfocus="this.placeholder = ''" onblur="this.placeholder = 'Data de Nascimento'" required value="<%=cli.getDataNascimento()%>"
															class="single-input">
									<input type="text" placeholder="CPF" name="txtCpf" id="txtCpf"
															onfocus="this.placeholder = ''" onblur="this.placeholder = 'CPF'" required value="<%=cli.getCpf()%>"
															class="single-input cpf">
									<input type="hidden" id="idCli" name="idCli" value="<%=cli.getId()%>">							
									<div class="mt-10">
										<div class="row">
											<div class="col-lg-12">
												<!-- <blockquote class="generic-blockquote">
														<center><h5>Telefone</h5></center>
														<div class="input-group-icon mt-10">
															<div class="form-select" id="default-select">
																<select>
																	<option selected="selected" value="Tipo">Tipo</option>
																	<option value="2">Residencial</option>
																	<option value="3">Celular</option>
																</select>
															</div>
														</div>
														<input type="text" id="txtDdd" name="txtDdd" placeholder="DDD"
															onfocus="this.placeholder = ''" onblur="this.placeholder = 'DDD'" required
															class="single-input">
														<input type="text" id="txtNumero" name="txtNumero" placeholder="N�mero"
															onfocus="this.placeholder = ''" onblur="this.placeholder = 'N�mero'" required
															class="single-input">
												</blockquote> -->
											</div>
										</div>
									</div>
									<div class="mt-10">
										<a class="btn_3" type="submit" id="OPERACAO" name="OPERACAO" href="customer-profile.jsp">Atualizar</a>
									</div>
								</div>
							</div>
						</div>
				</div>		
			</div>
		</div>
        <!--================register_part end =================-->
    </main>
    <footer>

    </footer>
    <!--? Search model Begin -->
    <!-- Search model end -->
    
    <!-- JS here -->

    <script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
    <!-- Jquery, Popper, Bootstrap -->
    <script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="./assets/js/popper.min.js"></script>
    <script src="./assets/js/bootstrap.min.js"></script>
    <!-- Jquery Mobile Menu -->
    <script src="./assets/js/jquery.slicknav.min.js"></script>

    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src="./assets/js/owl.carousel.min.js"></script>
    <script src="./assets/js/slick.min.js"></script>

    <!-- One Page, Animated-HeadLin -->
    <script src="./assets/js/wow.min.js"></script>
    <script src="./assets/js/animated.headline.js"></script>
    
    <!-- Scroll up, nice-select, sticky -->
    <script src="./assets/js/jquery.scrollUp.min.js"></script>
    <script src="./assets/js/jquery.nice-select.min.js"></script>
    <script src="./assets/js/jquery.sticky.js"></script>
    <script src="./assets/js/jquery.magnific-popup.js"></script>

    <!-- contact js -->
    <script src="./assets/js/contact.js"></script>
    <script src="./assets/js/jquery.form.js"></script>
    <script src="./assets/js/jquery.validate.min.js"></script>
    <script src="./assets/js/mail-script.js"></script>
    <script src="./assets/js/jquery.ajaxchimp.min.js"></script>
    
    <!-- Jquery Plugins, main Jquery -->	
    <script src="./assets/js/plugins.js"></script>
    <script src="./assets/js/main.js"></script>

</body>
    
</html>