<!doctype html>
<%@page import="modelo.dominio.EntidadeDominio"%>
<%@page import="modelo.dominio.Cliente"%>
<%@page import="modelo.dominio.Endereco"%>
<%@page import="modelo.dominio.CartaoCredito"%>
<%@page import="modelo.dominio.Resultado"%>
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Loja de Mang� - Perfil</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="<%=request.getContextPath()%>/assets/img/favicon.ico">

    <!-- CSS here -->
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/flaticon.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/slicknav.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/animate.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/themify-icons.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/slick.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/nice-select.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/style.css">
</head>
<body>
    <header>
        <!-- Header Start -->
        <div class="header-area">
            <div class="main-header header-sticky">
                <div class="container-fluid">
                    <div class="menu-wrapper">
                        <!-- Logo -->
                        <div class="logo">
                            <a href="index.jsp"><img class="manga-logo" src="assets/img/book-icon.png" alt=""></a>
                        </div>
                        <!-- Main-menu -->
                        <div class="main-menu d-none d-lg-block">
                            <nav>                                                
                                <ul id="navigation">  
                                    <li><a href="<%=request.getContextPath()%>/index.jsp">In�cio</a></li>
                                    <li><a href="<%=request.getContextPath()%>/shop.jsp">Produtos</a></li>
                                    <li><a href="#">P�ginas</a>
                                        <ul class="submenu">
	                                        <form action="Logout" method="delete">
	                                            <li><a type="submit" href="">Sair</a></li>                                  
	                                        </form>
                                            <li><a href="<%=request.getContextPath()%>/autenticado/cart.jsp">Carrinho</a></li>
                                            <li><a href="<%=request.getContextPath()%>/autenticado/confirmation.jsp">Confirma��o</a></li>
                                            <li><a href="<%=request.getContextPath()%>/autenticado/checkout.jsp">Checkout</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <!-- Header Right -->
                        <div class="header-right">
                            <ul>
                                <li> <a href="<%=request.getContextPath()%>/login.jsp"><span class="flaticon-user"></span></a></li>
                                <li><a href="<%=request.getContextPath()%>/autenticado/cart.jsp"><span class="flaticon-shopping-cart"></span></a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Mobile Menu -->
                    <div class="col-12">
                        <div class="mobile_menu d-block d-lg-none"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header End -->
    </header>
    
    <% 
    	Resultado resultado = (Resultado) session.getAttribute("resultado");
    	Cliente cliente = (Cliente) session.getAttribute("clienteLogado");
    %>
    
    <main>
        <section class="button-area">
            <div class="container">
                <h3>Bem vindo, <%=cliente.getNomeCompleto()%></h3>
                <div class="row">
                	<a href="<%=request.getContextPath()%>/autenticado/address-insert.jsp" class="genric-btn info e-large">Adicionar Endere�o</a>
                	<a href="<%=request.getContextPath()%>/autenticado/credit-card-insert.jsp" class="genric-btn info e-large">Adicionar Cart�o</a>
                	<form action="VisualizarCliente" method="post">
                		<input type="hidden" id="idCli" name="idCli" value="<%= cliente.getId()%>">
                    	<button value="VISUALIZAR" id="OPERACAO" name="OPERACAO" class="genric-btn info e-large">Visualizar dados</button>
                	</form>
                	<a href="<%=request.getContextPath()%>/autenticado/orders-list.jsp" class="genric-btn info e-large">Meus Pedidos</a>
                </div>
            </div>
            
            <div class="container">
            	<h3>Endere�os:</h3>
            	<table class="table">
	            	<thead>
						<th scope="col">Logradouro/Numero</th>
						<th scope="col">Complemento</th>
						<th scope="col">CEP</th>
						<th scope="col">Bairro/Cidade(Estado)</th>
						<th scope="col">Tipo</th>
						<th scope="col">Editar</th>
						
					</thead>
					<tbody>
						<%
							for (EntidadeDominio i : cliente.getEndereco()) {
								Endereco end = (Endereco) i;
						%>
							<tr>
								<td scope="row"><%=end.getLogradouro()%>, <%=end.getNumero() %></td>
								<td scope="row"><%=end.getComplemento()%></td>
								<td scope="row"><%=end.getCep()%></td>
								<td scope="row"><%=end.getBairro()%> - <%=end.getCidade() %>(<%=end.getEstado() %>)</td>
								<td scope="row"><%=end.getTipo()%></td>
								<td scope="row">
									<form action="AlterarEndereco" method="post">
										<input type="hidden" id="idEnd" name="idEnd" value="<%= end.getId()%>">
										<a href="autenticado/edit-address.jsp" id="OPERACAO" name="OPERACAO" class="genric-btn info e-large">Alterar</a>
									</form>
								</td>
							</tr>
						<%
							}
						%>
					</tbody>
				</table>
            </div>
            
            <div class="container">
            	<h3>Cart�es de Cr�dito:</h3>
            	<table class="table">
	            	<thead>
						<th scope="col">N�mero do Cart�o</th>
						<th scope="col">Bandeira</th>
						<th scope="col">Nome Impresso</th>
						<th scope="col">Validade</th>
						<th scope="col">C�digo de Seguran�a</th>
						<th scope="col">Editar</th>
					</thead>
					<tbody>
						<%
							for (EntidadeDominio i : cliente.getCartaoCredito()) {
	
								CartaoCredito cc = (CartaoCredito) i;
								System.out.println("Validadde em String: " + cc.getValidade());
						%>
							<tr>
								<td scope="row"><%=cc.getNumeroCartao() %></td>
								<td scope="row"><%=cc.getBandeira() %></td>
								<td scope="row"><%=cc.getNomeImpresso() %></td>
								<td scope="row"><%=cc.getValidade() %></td>
								<td scope="row"><%=cc.getCodigoSeguranca() %></td>
								<td scope="row">
								
								<input type="hidden" id="idCartao" name="idCartao" value="<%= cc.getId()%>">
								<a id="OPERACAO" name="OPERACAO" class="genric-btn info e-large" href="autenticado/edit-credit-card.jsp">Alterar</a>
								
								</td>
							</tr>
						<%
							}
						%>
					</tbody>
				</table>
            </div>
            
        </section>
    </main>
    <footer>

    </footer>
    <!--? Search model Begin -->
    <!-- Search model end -->
    
    <!-- JS here -->

    <script src="<%=request.getContextPath()%>/assets/js/vendor/modernizr-3.5.0.min.js"></script>
    <!-- Jquery, Popper, Bootstrap -->
    <script src="<%=request.getContextPath()%>/assets/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="<%=request.getContextPath()%>/assets/js/popper.min.js"></script>
    <script src="<%=request.getContextPath()%>/assets/js/bootstrap.min.js"></script>
    <!-- Jquery Mobile Menu -->
    <script src="<%=request.getContextPath()%>/assets/js/jquery.slicknav.min.js"></script>

    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src="<%=request.getContextPath()%>/assets/js/owl.carousel.min.js"></script>
    <script src="<%=request.getContextPath()%>/assets/js/slick.min.js"></script>

    <!-- One Page, Animated-HeadLin -->
    <script src="<%=request.getContextPath()%>/assets/js/wow.min.js"></script>
    <script src="<%=request.getContextPath()%>/assets/js/animated.headline.js"></script>
    
    <!-- Scroll up, nice-select, sticky -->
    <script src="<%=request.getContextPath()%>/assets/js/jquery.scrollUp.min.js"></script>
    <script src="<%=request.getContextPath()%>/assets/js/jquery.nice-select.min.js"></script>
    <script src="<%=request.getContextPath()%>/assets/js/jquery.sticky.js"></script>
    <script src="<%=request.getContextPath()%>/assets/js/jquery.magnific-popup.js"></script>

    <!-- contact js -->
    <script src="<%=request.getContextPath()%>/assets/js/contact.js"></script>
    <script src="<%=request.getContextPath()%>/assets/js/jquery.form.js"></script>
    <script src="<%=request.getContextPath()%>/assets/js/jquery.validate.min.js"></script>
    <script src="<%=request.getContextPath()%>/assets/js/mail-script.js"></script>
    <script src="<%=request.getContextPath()%>/assets/js/jquery.ajaxchimp.min.js"></script>
    
    <!-- Jquery Plugins, main Jquery -->	
    <script src="<%=request.getContextPath()%>/assets/js/plugins.js"></script>
    <script src="<%=request.getContextPath()%>/assets/js/main.js"></script>
    
    <!-- JQuery Mask -->
	<script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/mask/dist/jquery.mask.min.js"></script>
</body>
    
</html>