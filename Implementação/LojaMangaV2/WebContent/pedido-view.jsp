<%@page import="modelo.dominio.Pedido"%>
<%@page import="modelo.dominio.Cliente"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Loja de Mang� - Visualizar Pedido</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

    <!-- CSS here -->
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/flaticon.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/slicknav.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/animate.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/themify-icons.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/slick.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/nice-select.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/style.css">
<body>
    <header>
        <!-- Header Start -->
        <div class="header-area">
            <div class="main-header header-sticky">
                <div class="container-fluid">
                    <div class="menu-wrapper">
                        <!-- Logo -->
                        <div class="logo">
                            <a href="<%=request.getContextPath()%>/index.jsp"><img class="manga-logo" src="assets/img/book-icon.png" alt=""></a>
                        </div>
                        <!-- Main-menu -->
                        <div class="main-menu d-none d-lg-block">
                            <nav>                                                
                                <ul id="navigation">  
                                    <li><a href="<%=request.getContextPath()%>/index.jsp">In�cio</a></li>
                                    <li><a href="<%=request.getContextPath()%>/shop.jsp">Produtos</a></li>
                                    <li><a href="#">P�ginas</a>
                                        <ul class="submenu">
                                            <li><a href="<%=request.getContextPath()%>/login.jsp">Login</a></li>
                                            <li><a href="<%=request.getContextPath()%>/autenticado/cart.jsp">Carrinho</a></li>
                                            <li><a href="<%=request.getContextPath()%>/autenticado/confirmation.jsp">Confirma��o</a></li>
                                            <li><a href="<%=request.getContextPath()%>/autenticado/checkout.jsp">Checkout</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <!-- Header Right -->
                        <div class="header-right">
                            <ul>
                                <li> <a href="<%=request.getContextPath()%>/login.jsp"><span class="flaticon-user"></span></a></li>
                                <li><a href="<%=request.getContextPath()%>/autenticado/cart.jsp"><span class="flaticon-shopping-cart"></span></a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Mobile Menu -->
                    <div class="col-12">
                        <div class="mobile_menu d-block d-lg-none"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header End -->
    </header>
    
    <% 
    	Pedido ped = (Pedido) session.getAttribute("VisualizarPed");
    %>
    
    <main>
        <section class="button-area">
            <div class="container">
                <h3>Informa�oes do Pedido</h3>
                <div class="row">
                    
					<%
					Cliente cli = (Cliente) session.getAttribute("clienteLogado"); 
					if (cli.getAdmin() == 1) { %>
					<div class="input-group-icon mt-10">
						<select class="form-select" id="selectEstado" name="selectEstado">
							<option selected="selected" value="Status">Status</option>
							<option value="AC">Aprovada</option>
							<option value="AL">Reprovada</option>
							<option value="AP">Em processamento</option>
							<option value="AM">Em troca</option>
							<option value="BA">Troca autorizada</option>
							<option value="CE">Em tr�nsito</option>
							<option value="ES">Entregue</option>
						</select>
					</div>
					<a href="<%=request.getContextPath()%>/autenticado/admin/orders-list-admin.jsp" class="btn_3" id="OPERACAO" name="OPERACAO">Confirmar</a>
					<%
						}
					%>
                   	<form action="ExcluirPedido" method="post">
                   		<input type="hidden" id="idPed" name="idPed" value="5">
                   		<button class="genric-btn danger e-large" type="submit" id="OPERACAO" name="OPERACAO" value="EXCLUIR"> Excluir Pedido</button>
                   	</form>
                </div>
            </div>
        </section>
        <div class="container">
	        <div class="mt-10">
	        	<h3>One Piece - Volume 95<h3>
	        </div>
	    </div>
    </main>
    <footer>

    </footer>
    <!--? Search model Begin -->
    <!-- Search model end -->
    
    <!-- JS here -->

    <script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
    <!-- Jquery, Popper, Bootstrap -->
    <script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="./assets/js/popper.min.js"></script>
    <script src="./assets/js/bootstrap.min.js"></script>
    <!-- Jquery Mobile Menu -->
    <script src="./assets/js/jquery.slicknav.min.js"></script>

    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src="./assets/js/owl.carousel.min.js"></script>
    <script src="./assets/js/slick.min.js"></script>

    <!-- One Page, Animated-HeadLin -->
    <script src="./assets/js/wow.min.js"></script>
    <script src="./assets/js/animated.headline.js"></script>
    
    <!-- Scroll up, nice-select, sticky -->
    <script src="./assets/js/jquery.scrollUp.min.js"></script>
    <script src="./assets/js/jquery.nice-select.min.js"></script>
    <script src="./assets/js/jquery.sticky.js"></script>
    <script src="./assets/js/jquery.magnific-popup.js"></script>

    <!-- contact js -->
    <script src="./assets/js/contact.js"></script>
    <script src="./assets/js/jquery.form.js"></script>
    <script src="./assets/js/jquery.validate.min.js"></script>
    <script src="./assets/js/mail-script.js"></script>
    <script src="./assets/js/jquery.ajaxchimp.min.js"></script>
    
    <!-- Jquery Plugins, main Jquery -->	
    <script src="./assets/js/plugins.js"></script>
    <script src="./assets/js/main.js"></script>

</body>
    
</html>