<%@page import="modelo.dominio.Carrinho"%>
<%@page import="modelo.dominio.EntidadeDominio"%>
<%@page import="modelo.dao.MangaDAO"%>
<%@page import="modelo.dominio.Resultado"%>
<%@page import="modelo.dominio.Manga"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!doctype html>
<html class="no-js" lang="zxx">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Loja de Mang�</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="site.webmanifest">
<link rel="shortcut icon" type="image/x-icon"
	href="assets/img/favicon.ico">

<!-- CSS here -->
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/flaticon.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/slicknav.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/animate.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/themify-icons.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/slick.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/nice-select.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/style.css">
</head>

<body>
	<!--? Preloader Start -->
	<div id="preloader-active">
		<div
			class="preloader d-flex align-items-center justify-content-center">
			<div class="preloader-inner position-relative">
				<div class="preloader-circle"></div>
				<div class="preloader-img pere-text">
					<img src="assets/img/book-icon.png" alt="">
				</div>
			</div>
		</div>
	</div>
	<!-- Preloader Start -->
	<header>
		<!-- Header Start -->
		<div class="header-area">
			<div class="main-header header-sticky">
				<div class="container-fluid">
					<div class="menu-wrapper">
						<!-- Logo -->
						<div class="logo">
							<a href="<%=request.getContextPath()%>/index.jsp"><img class="manga-logo"
								src="assets/img/book-icon.png" alt=""></a>
						</div>
						<!-- Main-menu -->
						<div class="main-menu d-none d-lg-block">
							<nav>
								<ul id="navigation">
									<li><a href="<%=request.getContextPath()%>/index.jsp">Inicio</a></li>
									<li><a href="<%=request.getContextPath()%>/shop.jsp">Produtos</a></li>
								</ul>
							</nav>
						</div>

						<%
							if (session.getAttribute("carrinho") != null) {

							Carrinho carrinho = (Carrinho) session.getAttribute("carrinho");

						} else {
							Carrinho carrinho = new Carrinho();
						}
						%>

						<!-- Header Right -->
						<div class="header-right">
							<ul>
								<%
									if (session.getAttribute("clienteLogado") != null) {
								%>
									<li><a href="<%=request.getContextPath()%>/autenticado/customer-profile.jsp"><span class="flaticon-user"></span></a></li>
								<%
									}else{
								%>
									<li><a href="<%=request.getContextPath()%>/login.jsp"><span class="flaticon-user"></span></a></li>
								<%
									}
								%>
								
								<li><a href="<%=request.getContextPath()%>/autenticado/cart.jsp"><span
										class="flaticon-shopping-cart"></span></a></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- Mobile Menu -->
				<div class="col-12">
					<div class="mobile_menu d-block d-lg-none"></div>
				</div>
			</div>
		</div>
		</div>
		<!-- Header End -->
	</header>
	<main>
		<div class="popular-items section-padding30">
			<div class="container">
				<!-- Section tittle -->
				<div class="row">
					<div class="col-xl-12">
						<div class="section-tittle mb-70">
							<h2 class="text-center">Loja de Manga</h2>
						</div>
					</div>
				</div>
			</div>

			<%
				Manga man = new Manga();
			MangaDAO mgDao = new MangaDAO();
			man.setId(-1);
			Resultado resultado = mgDao.consultar(man);
			%>
			<div class="container">
				<div class="row">

					<%
						for (EntidadeDominio ent : resultado.getEntidades()) {
						Manga mg = (Manga) ent;

						if (mg.getAtivo().equals("ATIVO")) {
					%>
					<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
						<a
							href="VisualizarManga?idMg=<%=mg.getId()%>&OPERACAO=VISUALIZAR&PAGINA=cliente">
							<div class="single-popular-items mb-50 text-center">
								<div class="popular-img">
									<img
										src="<%=request.getContextPath()%>/assets/img/<%=mg.getEndereco_imagem()%>"
										alt="">
									<div class="img-cap">
										<form action="AdicionarItemCarrinho" method="post">
											<input type="hidden" value="<%=mg%>" id="mg" name="mg">
											<input type="hidden" value="Carrinho" id="PAGINA"
												name="PAGINA"> <input type="hidden" value="1"
												id="txtQntde" name="txtQntde">
											<button type="submit" id="OPERACAO" name="OPERACAO"
												value="SALVAR">
												<span>Adicionar ao carrinho</span>
											</button>
										</form>
									</div>
								</div>
								<div class="popular-caption">
									<h3><%=mg.getTitulo()%>
										- Volume
										<%=mg.getEdicao()%></h3>
									<span>R$ <%=mg.getPreco()%></span>
								</div>
							</div>
						</a>
					</div>
					<%
						}
					}
					%>
					<!-- Button -->
				</div>
				<div class="row justify-content-center">
					<div class="room-btn pt-70">
						<a href="shop.jsp" class="btn view-btn1">Ver mais...</a>
					</div>
				</div>
			</div>
			<!-- Popular Items End -->
			<!--? Shop Method Start-->
			<div class="shop-method-area mt-5">
				<div class="container">
					<div class="method-wrapper">
						<div class="row d-flex justify-content-between">
							<div class="col-xl-4 col-lg-4 col-md-6">
								<div class="single-method mb-40">
									<i class="ti-package"></i>
									<h6>Acompanhamento de entrega</h6>
									<p>Acompanhe o Status da sua entrega</p>
								</div>
							</div>
							<div class="col-xl-4 col-lg-4 col-md-6">
								<div class="single-method mb-40">
									<i class="ti-unlock"></i>
									<h6>Pagamento em cart�o seguro</h6>
									<p>Pague com seu cart�o de cr�dito</p>
								</div>
							</div>
							<div class="col-xl-4 col-lg-4 col-md-6">
								<div class="single-method mb-40">
									<i class="ti-reload"></i>
									<h6>Sistema de Devolu��o</h6>
									<p>Ocorreu algum problema ? Solicite a devolu��o do produto</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Shop Method End-->
	</main>
	<!-- JS here -->
	<script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
	<!-- Jquery, Popper, Bootstrap -->
	<script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
	<script src="./assets/js/popper.min.js"></script>
	<script src="./assets/js/bootstrap.min.js"></script>
	<!-- Jquery Mobile Menu -->
	<script src="./assets/js/jquery.slicknav.min.js"></script>

	<!-- Jquery Slick , Owl-Carousel Plugins -->
	<script src="./assets/js/owl.carousel.min.js"></script>
	<script src="./assets/js/slick.min.js"></script>

	<!-- One Page, Animated-HeadLin -->
	<script src="./assets/js/wow.min.js"></script>
	<script src="./assets/js/animated.headline.js"></script>
	<script src="./assets/js/jquery.magnific-popup.js"></script>

	<!-- Scrollup, nice-select, sticky -->
	<script src="./assets/js/jquery.scrollUp.min.js"></script>
	<script src="./assets/js/jquery.nice-select.min.js"></script>
	<script src="./assets/js/jquery.sticky.js"></script>

	<!-- contact js -->
	<script src="./assets/js/contact.js"></script>
	<script src="./assets/js/jquery.form.js"></script>
	<script src="./assets/js/jquery.validate.min.js"></script>
	<script src="./assets/js/mail-script.js"></script>
	<script src="./assets/js/jquery.ajaxchimp.min.js"></script>

	<!-- Jquery Plugins, main Jquery -->
	<script src="./assets/js/plugins.js"></script>
	<script src="./assets/js/main.js"></script>

</body>
</html>